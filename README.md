# platform-kiki-rtn

test

## Installation

```sh
npm install platform-kiki-rtn
```

## Usage

```js
import { multiply } from 'platform-kiki-rtn';

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT

---

Made with [create-react-native-library](https://github.com/callstack/react-native-builder-bob)


``` bash
npm install -g git@gitlab.com:Keccak256-evg/fe-platform/templatecli/kikicli.git
npm install -g git@gitlab.com:Keccak256-evg/fe-platform/lowcode/openapi-typescript-codegen.git
cd ./dev-modules/platformkikirtn
kkrn apijson -o ../rtncodegen/testjson.json

cd ..
openapi --input ./rtncodegen/testjson.json --output ./rtncodegen/rtnnetwoek --name RtnClient --type rtn

````