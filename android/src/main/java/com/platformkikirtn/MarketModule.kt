package com.platformkikirtn

import android.annotation.SuppressLint
import android.util.Log
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.kikitrade.dao.ConnectResult
import com.kikitrade.dao.RefreshNoDataException
import com.kikitrade.dao.Zone
import com.kikitrade.dao.market.model.SymbolStat
import com.kikitrade.dao.market.model.legacy.*
import com.kikitrade.dao.market.websocket.model.DepthWs
import com.kikitrade.dao.market.websocket.model.OrderBookWs
import com.kikitrade.dao.market.websocket.model.OrderWs
import com.kikitrade.framework.coroutine.PageScopeManager
import com.kikitrade.framework.generateUUID
import com.kikitrade.platform.ApplicationInitializer
import com.kikitrade.platform.container.*
import com.platformkikirtn.utils.ToJsHelper
import com.platformkikirtn.utils.ToJsHelper.Companion.MARKET_CONNECT_TO_NATIVE_EVENT
import com.platformkikirtn.utils.ToJsHelper.Companion.REMOTE_DATE_UPDATE_EVENT
import io.ktor.utils.io.*
import kotlinx.coroutines.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.collect
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class MarketModule internal constructor(
  val reactContext: ReactApplicationContext,
  val application: ApplicationContainer
) : MarketModuleSpec(reactContext) {
  private val marketDataEventEmitter: DeviceEventManagerModule.RCTDeviceEventEmitter =
    reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
  var symbols: String? = null
  var tickers: String? = null
  var zones: String? = null
  var currencies: String? = null

  init {
    symbols = ApplicationInitializer.preloadSymbols
    tickers = ApplicationInitializer.preloadTickers
    zones = ApplicationInitializer.preloadZones
    currencies = ApplicationInitializer.preloadCurrencies
  }


  override fun getName(): String {
    return NAME
  }

  @ReactMethod
  override fun getOfflineMarketDataDetail(scopeId: String, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = scope.let { application.marketDetailRepository(it) }
    scope.launch {
      val eventId = "EventUpdateId-${generateUUID()}"
      val localData = marketDetailRepository.getLocalMarketDetailsByState(null,SymbolStat.OFFLINE)
      val toJsLocalData = ToJsHelper.convertToEventUpdateJsArray(localData, eventId, ::formatMarketDataToWritableMap)
      promise.resolve(toJsLocalData)
      val remoteData = marketDetailRepository.getRemoteMarketDetailsThenFilterByState(null,SymbolStat.OFFLINE)
      val toJsRemoteData = ToJsHelper.convertToEventUpdateJsArray(
        remoteData,
        eventId,
        ::formatMarketDataToWritableMap
      )
      marketDataEventEmitter.emit(REMOTE_DATE_UPDATE_EVENT, toJsRemoteData)
    }
  }


  @ReactMethod
  override fun getMarketData(
    scopeId: String,
    symbolNames: ReadableArray?,
    promise: Promise
  ) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = scope.let { application.marketDetailRepository(it) }
    scope.launch {
      val symbolNameStrList = symbolNames?.toArrayList()?.filterIsInstance<String>()
      // get all marketData
      val dataList = marketDetailRepository.getMarketData(symbolNameStrList,SymbolStat.NORMAL)
      val toJsDate = ToJsHelper.convertToJsArray(dataList, ::formatMarketDataToWritableMap)
      promise.resolve(toJsDate)

    }
  }

  @ReactMethod
  override fun connectMarketData(scopeId: String, symbolNames: ReadableArray?, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    scope.launch {
      val symbolNameStrList = symbolNames?.toArrayList()?.filterIsInstance<String>()
      val result = ConnectResult<List<MarketData>>(scope)
      marketDetailRepository.connect(symbolNameStrList,result)
      val eventId = result.eventId
      promise.resolve(eventId)
      result.flowData.collect { value ->
        val toJsData = ToJsHelper.convertToEventUpdateJsArray(value,eventId, ::formatMarketDataToWritableMap)
        marketDataEventEmitter.emit(MARKET_CONNECT_TO_NATIVE_EVENT, toJsData)
      }
    }
  }


  @SuppressLint("LongLogTag")
  @ReactMethod
  override fun preloadMarketLocalDB(promise: Promise) {
    val scopeId = UUID.randomUUID().toString()
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val tickerRepository = application.tickerRepository(scope)
    val symbolRepository = application.symbolRepository(scope)
    val zoneRepository = application.zoneRepository(scope)
    val currencyRepository = application.currencyRepository(scope)
    scope.launch {
      try {
        symbolRepository.takeIf { it.checkIsDBPreload() } ?: symbolRepository.preloadSymbols(
          symbols ?: "{}"
        )
        tickerRepository.takeIf { it.checkIsDBPreload() } ?: tickerRepository.preloadTickers(
          tickers ?: "{}"
        )
        zoneRepository.takeIf { it.checkIsDBPreload() } ?: zoneRepository.preloadZones(
          zones ?: "{}"
        )
        currencyRepository.takeIf { it.checkIsDBPreload() } ?: currencyRepository.preloadCurrencies(
          currencies ?: "{}"
        )
        promise.resolve(true)
      } catch (e: RefreshNoDataException) {
        Log.e(NAME, "preloadMarketLocalDB:-- RefreshNoDataException--")
        promise.resolve(false)
      } catch (e: TimeoutCancellationException) {
        Log.e(NAME, "preloadMarketLocalDB:-- TimeoutCancellationException--")
        promise.resolve(false)
      } catch (e: CancellationException) {
        Log.e(NAME, "preloadMarketLocalDB:-- CancellationException--")
        promise.resolve(false)
      }
    }
  }

  @ReactMethod
  override fun connectMarketDataDetails(
    scopeId: String,
    symbolNames: ReadableArray?,
    promise: Promise
  ) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    scope.launch {
      val symbolNameStrList = symbolNames?.toArrayList()?.filterIsInstance<String>()
      val result = ConnectResult<List<MarketDataDetail>>(scope)
      marketDetailRepository.connectDetail(symbolNameStrList,result)
      val eventId = result.eventId
      promise.resolve(eventId)
      result.flowData.collect { value ->
        val toJsData = ToJsHelper.convertToEventUpdateJsArray(value,eventId, ::formatMarketDataToWritableMap)
        marketDataEventEmitter.emit(MARKET_CONNECT_TO_NATIVE_EVENT, toJsData)
      }
    }
  }

  @ReactMethod
  override fun getMarketDataDetail(scopeId: String, symbolNames: ReadableArray?, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = scope.let { application.marketDetailRepository(it) }
    scope.launch {
      val symbolNameStrList = symbolNames?.toArrayList()?.filterIsInstance<String>()
      val eventId = "EventUpdateId-${generateUUID()}"
      val localData = marketDetailRepository.getLocalMarketDetailsByState(symbolNameStrList,SymbolStat.NORMAL,SymbolStat.INNER_TEST,SymbolStat.EXPLORE)
      val toJsLocalData =
        ToJsHelper.convertToEventUpdateJsArray(localData, eventId, ::formatMarketDataToWritableMap)
      promise.resolve(toJsLocalData)
      val remoteData = marketDetailRepository.getRemoteMarketDetailsThenFilterByState(symbolNameStrList,SymbolStat.NORMAL,SymbolStat.INNER_TEST,SymbolStat.EXPLORE)
      val toJsRemoteData = ToJsHelper.convertToEventUpdateJsArray(remoteData,eventId, ::formatMarketDataToWritableMap)
      marketDataEventEmitter.emit(REMOTE_DATE_UPDATE_EVENT, toJsRemoteData)
    }
  }

  @ReactMethod
  override fun getZoneListDetails(scopeId: String, maxRefreshTimeout: Double, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val zoneRepository = application.zoneRepository(scope)
    scope.launch {
      val result = zoneRepository.getZoneListDetails(maxRefreshTimeout.toInt())
      if (result == null) {
        promise.resolve(ToJsHelper.makeErrorRespond())
        return@launch
      }
      val zoneList = ToJsHelper.convertToJsArray(result, ::formatZoneListWritableMap)
      promise.resolve(zoneList)
    }
  }

  @ReactMethod
  override fun getMarketDetailTabZoneDataList(scopeId: String, num: Int, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val zoneRepository = scope.let { application.zoneRepository(it) }
    scope.launch {
      val data = zoneRepository.getMarketDetailTabZoneDataList(num ?: 3)
      promise.resolve(ToJsHelper.convertToJsArray(data, ::formatMarketTabInfoToWritableMap))
    }
  }

  @ReactMethod
  override fun getMarketDetailByCondition(
    scopeId: String,
    baseCurrency: String,
    zoneKey: String,
    sortName: String?,
    sortType: String,
    promise: Promise
  ) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = scope.let { application.marketDetailRepository(it) }
    scope.launch {
      val eventId = "EventUpdateId-${generateUUID()}"
      val localData = marketDetailRepository.getLocalMarketDetailsByCondition(
        baseCurrency,
        zoneKey,
        sortName,
        sortType
      )
      val toJsLocalData =
        ToJsHelper.convertToEventUpdateJsArray(localData, eventId, ::formatMarketDataToWritableMap)
      promise.resolve(toJsLocalData)
      val remoteUpdatedData = marketDetailRepository.getRemoteMarketDetailsByCondition(
        baseCurrency,
        zoneKey,
        sortName,
        sortType
      )
      val toJsRemoteData = ToJsHelper.convertToEventUpdateJsArray(
        remoteUpdatedData,
        eventId,
        ::formatMarketDataToWritableMap
      )
      marketDataEventEmitter.emit(REMOTE_DATE_UPDATE_EVENT, toJsRemoteData)
    }
  }

  @ReactMethod
  override fun getZoneDetail(
    scopeId: String,
    zoneKey: String,
    sortName: String,
    sortType: String,
    promise: Promise
  ) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    val zoneRepository = application.zoneRepository(scope)
    scope.launch {
      // 静态数据
      val localData = zoneRepository.getZoneDetail(zoneKey, sortName, sortType)
      val symbols = marketDetailRepository.getSymbols(localData.symbolList)
      val map = zoneRepository.getZoneListMap(localData.symbolList)
      val result = ToJsHelper.convertToJsMap(localData, ::formatZoneDetail)
      promise.resolve(result)
    }
  }

  @ReactMethod
  override fun connectZoneDetail(scopeId: String, zoneKey: String, sortName: String, sortType: String, promise: Promise){
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    val zoneRepository = application.zoneRepository(scope)
    scope.launch {
      // 静态数据
      val localData = zoneRepository.getZoneDetail(zoneKey, sortName, sortType)
      val symbols = marketDetailRepository.getSymbols(localData.symbolList)
      val map = zoneRepository.getZoneListMap(localData.symbolList)
      // 头部动态数据
      val result = ConnectResult<List<MarketDataDetail>>(scope)
      marketDetailRepository.connectDetail(symbols,result)
      val eventId = result.eventId
      promise.resolve(eventId)
      result.flowData.collect {
        // 发送事件
        it.forEach { detail ->
          if (map?.containsKey(detail.currency) == true) {
            map[detail.currency] = detail
          }
        }
        val zoneDetail = ZoneDetail(
          header = zoneRepository.exchangeZoneDetailHeader(map?.values?.toList()),
          map?.values?.toList()
        )
        val dynamic = ToJsHelper.convertToEventUpdateJsMap(zoneDetail,eventId ,::formatZoneDetail)
        marketDataEventEmitter.emit(MARKET_CONNECT_TO_NATIVE_EVENT, dynamic)
      }
    }
  }

  @ReactMethod
  override fun getZoneDetailSymbols(
    scopeId: String,
    zoneKey: String,
    sortName: String,
    sortType: String,
    promise: Promise
  ) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val zoneRepository = application.zoneRepository(scope)
    scope.launch {
      val symbols = zoneRepository.getZoneDetail(zoneKey, sortName, sortType).symbolList
      val toJsData = ToJsHelper.convertToJsArray(symbols, ::formatMarketDataToWritableMap)
      promise.resolve(toJsData)
    }
  }

  @ReactMethod
  override fun connectOrderBook(scopeId: String, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    scope.launch {
      val result = ConnectResult<OrderBookWs>(scope)
      marketDetailRepository.connectOrderBook(result)
      val eventId = result.eventId
      promise.resolve(eventId)
      result.flowData.collect{value ->
        println("connectOrderBook ==== $value")
        val toJsData = ToJsHelper.convertToEventUpdateJsMap(value,eventId, ::formatOrderBookDataToWritableMap)
        marketDataEventEmitter.emit(MARKET_CONNECT_TO_NATIVE_EVENT, toJsData)
      }
    }
  }

  @ReactMethod
  override fun connectDepth(scopeId: String, promise: Promise) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    scope.launch {
      val result = ConnectResult<DepthWs>(scope)
      marketDetailRepository.connectDepth(result)
      val eventId = result.eventId
      promise.resolve(eventId)
      result.flowData.collect{value ->
        val toJsData = ToJsHelper.convertToEventUpdateJsMap(value,eventId, ::formatDepthDataToWritableMap)
        marketDataEventEmitter.emit(MARKET_CONNECT_TO_NATIVE_EVENT, toJsData)
      }
    }
  }

  @ReactMethod
  override fun connectOrder(scopeId: String, promise: Promise) {
    println("order socket调试 bridge ==== call connectOrder")
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketDetailRepository = application.marketDetailRepository(scope)
    scope.launch {
      val result = ConnectResult<OrderWs>(scope)
      marketDetailRepository.connectOrder(result)
      val eventId = result.eventId
      promise.resolve(eventId)
      result.flowData.collect{ value ->
        println("order socket调试 bridge flowData value==== $value")
        val toJsData = ToJsHelper.convertToEventUpdateJsMap(value,eventId, ::formatOrderDataToWritableMap)
        marketDataEventEmitter.emit(MARKET_CONNECT_TO_NATIVE_EVENT, toJsData)
      }
    }
  }

  @ReactMethod
  override fun sendMsgToWebsocket(scopeId: String, msg: ReadableMap) {
    val scope = PageScopeManager.getOrCreatePageScope(scopeId)
    val marketSocketRepository = application.marketSocketRepository()
    val msgHashMap = msg.toHashMap()
    val msgMap = msgHashMap.map { e -> e.key to e.value.toString() }.toMap()
    scope.launch{
      try {
        marketSocketRepository.sendRoomMsg(msgMap)
      }catch (e:Exception){
        e.printStackTrace()
        println("sendMsgToWebsocket error cause:${e.cause}")
        println("sendMsgToWebsocket error msg:${e.message}")
      }
    }
  }


  private fun formatZoneDetail(localData: ZoneDetail): WritableMap {
    val map = Arguments.createMap()
    val headerMap = formatZoneHeader(localData.header)
    map.putArray("symbolList", ToJsHelper.convertListDataToWritableArray(localData.symbolList,::formatMarketDataToWritableMap))
    map.putMap("header", headerMap)
    return map
  }

  private fun formatZoneHeader(header: ZoneHeader): WritableMap {
    val map = Arguments.createMap()
    map.apply {
      putDouble("allRiseAndFall", header.allRiseAndFall)
      putInt("riseCount", header.riseCount ?: 0)
      putInt("fallCount", header.fallCount ?: 0)
      putString("leadingCurrency", header.leadingCurrency)
      putDouble("leadingCurrencyRise", header.leadingCurrencyRise ?: 0.0)
    }
    return map
  }

  /**
   *  构造返回值
   */
  private fun formatZoneListWritableMap(zoneBean: ZoneBean): WritableMap {
    val map = Arguments.createMap();
    map.apply {
      putString("key", zoneBean.key)
      putString("name", zoneBean.name)
      putString("leadingCurrency", zoneBean.leadingCurrency)
      putInt("order", zoneBean.order.toInt())
      putInt("status", zoneBean.status.toInt())
      putInt("fallCount", zoneBean.fallCount ?: 0)
      putInt("riseCount", zoneBean.riseCount ?: 0)
      putDouble("percent", zoneBean.percent ?: 0.0)
      putDouble("leadingPercent", zoneBean.leadingPercent ?: 0.0)
    }

    val picPaths = Arguments.createArray()
    zoneBean.picPaths.forEach {
      picPaths.pushString(it)
    }
    map.putArray("picPaths", picPaths)
    // 简易k线数据
    val bars = Arguments.createArray()
    zoneBean.bars?.forEach {
      bars.pushString(it)
    }
    map.putArray("bars", bars)
    return map
  }

  private fun formatMarketDataToWritableMap(data: MarketData): WritableMap {
    val map = Arguments.createMap();
    val symbolPrecision = data.symbolPrecision
    val symbolPrecisionFormatStr = "%.${symbolPrecision}f"
    map.apply {
      putString("symbol", data.coinCode)
      putString("coinCode", data.coinCode)
      putDouble("priceLast", String.format(symbolPrecisionFormatStr, data.priceLast).toDouble())
      putString("volume", String.format("%.2f", data.volume))
      putString("quoteVolume", String.format("%.2f", data.quoteVolume))
      putDouble("riseAndFall", String.format("%.2f", data.riseAndFall).toDouble())
      putDouble("highPrice", String.format(symbolPrecisionFormatStr, data.highPrice).toDouble())
      putDouble("lowPrice", String.format(symbolPrecisionFormatStr, data.lowPrice).toDouble())
      putDouble("baseVolume", data.baseVolume)
      putInt("priceLastRise", data.priceLastRise)
      putDouble("ask", data.ask)
      putDouble("bid", data.bid)
      putString("currency", data.currency)
      putString("quoteCurrency", data.quoteCurrency)
      putDouble("modified", data.modified.toDouble())
      putInt("precision", data.symbolPrecision.toInt())
      putInt("symbolPrecision", data.symbolPrecision.toInt())
      putInt("volumePrecision", data.volumePrecision.toInt())
      putInt("tradePrecision", data.tradePrecision.toInt())
      putInt("tradeInputPrecision", data.tradeInputPrecision.toInt())
      putInt("tradeVolumePrecision", data.tradeVolumePrecision.toInt())
      putDouble("marketFreezeBuffer", data.marketFreezeBuffer)
      putDouble("sort", data.sort.toDouble())
      putString("baseCurrencyKey", data.baseCurrency)
      putDouble("orderMax", data.orderMax)
      putDouble("orderMin", data.orderMin)
      putBoolean("allowTrade", data.allowTrade)
      putInt("state", data.state)
      if (data is MarketDataDetail) {
        putDouble("orderQuoteMin", data.orderQuoteMin)
        putDouble("orderBaseMax", data.orderBaseMax)
        putDouble("orderBaseMin", data.orderBaseMin)
        putDouble("orderQuoteMax", data.orderQuoteMax)
      }
    }

    val writableArray = Arguments.createArray()
    data.priceList?.forEach{ writableArray.pushDouble(it)}
    map.putArray("priceList", writableArray)
    return map
  }

  private fun formatMarketTabInfoToWritableMap(data: Zone): WritableMap {
    val map = Arguments.createMap();
    map.apply {
      putString("name", data.name)
      putString("key", data.key)
    }
    return map
  }


  private fun formatOrderBookDataToWritableMap(data: OrderBookWs): WritableMap {
    val map = Arguments.createMap();
    map.apply {
      putString("symbol", data.symbol)
      putString("values", data.values)
      putInt("level", data.level)
    }
    return map
  }

  private fun formatDepthDataToWritableMap(data: DepthWs): WritableMap {
    val map = Arguments.createMap();
    map.apply {
      putString("symbol", data.symbol)
      putString("values", data.values)
    }
    return map
  }

  private fun formatOrderDataToWritableMap(data: OrderWs): WritableMap {
    val map = Arguments.createMap();
    val precisionsMap = Arguments.createMap();
    precisionsMap.apply { putInt("tradeInputPrecision",data.precisions?.tradeInputPrecision ?: 2) }
    precisionsMap.apply { putInt("tradePrecision",data.precisions?.tradePrecision ?: 2) }
    precisionsMap.apply { putInt("tradeVolumePrecision",data.precisions?.tradeVolumePrecision ?: 2) }
    precisionsMap.apply { putInt("volumePrecision",data.precisions?.volumePrecision ?: 2) }

    map.apply {
      putDouble("averageTradePrice", data.averageTradePrice)
      putString("baseAccountId", data.baseAccountId)
      putString("baseCurrency", data.baseCurrency)
      putDouble("baseQty", data.baseQty)
      putString("businessType", data.businessType)
      putDouble("created", data.created.toDouble())
      putString("customerId", data.customerId)
      putDouble("fee", data.fee)
      putString("feeCurrency", data.feeCurrency)
      putDouble("filledQty", data.filledQty)
      putDouble("filledVolume", data.filledVolume)
      putInt("latestVersion", data.latestVersion)
      putBoolean("migration", data.migration)
      putDouble("modified", data.modified.toDouble())
      putString("orderBookId", data.orderBookId)
      putString("orderCancelStatus", data.orderCancelStatus)
      putString("orderId", data.orderId)
      putString("orderStatus", data.orderStatus)
      putString("orderType", data.orderType)
      putMap("precisions",precisionsMap)
      putDouble("price", data.price)
      putString("quoteAccountId", data.quoteAccountId)
      putString("quoteCurrency", data.quoteCurrency)
      putDouble("quoteQty", data.quoteQty)
      putDouble("remainingQty", data.remainingQty)
      putDouble("remainingVolume", data.remainingVolume)
      putString("saasId", data.saasId)
      putString("side", data.side)
      putString("symbol", data.symbol)
      putString("takenProcessId", data.takenProcessId)
      putString("type", data.type)
      putBoolean("updateAveragePrice", data.updateAveragePrice)
      putBoolean("updateFee", data.updateFee)
      putBoolean("updateOrderBookId", data.updateOrderBookId)
    }
    return map
  }

  companion object {
    const val NAME = "PlatformKikiRnMarketModule"
  }
}
