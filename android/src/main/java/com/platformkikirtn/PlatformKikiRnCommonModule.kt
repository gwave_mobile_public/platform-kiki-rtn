package com.platformkikirtn

import android.app.Application
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.kikitrade.framework.*
import com.kikitrade.framework.coroutine.PageScopeManager
import com.kikitrade.networking.ConnectivityStatusMonitor
import com.kikitrade.networking.NetworkChangeReceiver
import com.kikitrade.platform.ApplicationInitializer
import com.kikitrade.platform.container.ApplicationContainer
import com.platformkikirtn.utils.NetStateHelper
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.math.log

class PlatformKikiRnCommonModule internal constructor(
  val reactContext: ReactApplicationContext,
  val application: ApplicationContainer
) :
  PlatformKikiRtnSpec(reactContext) {

  init {
    emitter = reactContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
  }

  override fun getName(): String {
    return NAME
  }

  @ReactMethod
  override fun getOrCreatePageScope(promise: Promise) {
    // new uuid
    val uuid = UUID.randomUUID().toString()
    val pageScope = PageScopeManager.getOrCreatePageScope(uuid)
    promise.resolve(pageScope.id)
  }

  @ReactMethod
  fun destroyPageScope(pageScopeId: String, promise: Promise) {
    val pageScope = PageScopeManager.findPageScope(pageScopeId)
    pageScope?.cancel()
    promise.resolve(true)
  }

  @ReactMethod
  fun httpGet(pageScopeId: String, url: String, promise: Promise) {
    val pageScope = PageScopeManager.findPageScope(pageScopeId)
    pageScope?.launch {
      val result = get(url)
      promise.resolve(result)
    }
    promise.resolve("GET: $url")
  }

  /**
   * TODO implement this method
   */
  fun get(url: String): String {
    return "GET: $url"
  }

  @ReactMethod
  fun httpPost(pageScopeId: String, url: String, promise: Promise) {
    promise.resolve("POST: $url")
  }

  @ReactMethod
  override fun initApplicationEnv(type: String, params: ReadableMap) {
    val evnType = when (type) {
      "dev" -> EnvironmentType.DEV
      "beta" -> EnvironmentType.BETA
      "prod" -> EnvironmentType.PROD
      "mock" -> EnvironmentType.MOCK
      "prodgreen" -> EnvironmentType.PROD_GREEN
      else -> {
        EnvironmentType.DEV
      }
    }
    ApplicationInitializer.initialize(
      evnType,
      application,
      ConstantConfig(
        jwtToken = params.getString("jwtToken") ?: "",
        isInnerTest = params.getBoolean("innerTest"),
        deviceId = params.getString("deviceId")?:""
      )
    )
  }

  @ReactMethod
  override fun updateJwtToken(jwtToken: String?) {
    println("order socket调试 ==== updateJwtToken：${jwtToken}")
    ApplicationInitializer.Companion.jwtToken = jwtToken
  }

  @ReactMethod
  override fun updateInnerTest(status: Boolean) {
    InnerTestManager.updateInnerTest(status)
  }

  @ReactMethod
  override fun multiply(a: Double, b: Double, promise: Promise) {
    promise.resolve(a * b);
  }

  companion object {
    const val NAME = "PlatformKikiRnCommonModule"
    private var emitter:DeviceEventManagerModule.RCTDeviceEventEmitter? = null

    fun getNetStateWatcher():NetworkChangeReceiver {
      GlobalScope.launch {
        FrameworkContext.addListener {
          when(it){
            FrameworkEvent.NetworkDisconnected() ,
            FrameworkEvent.NetworkUnstable() ,
            FrameworkEvent.NetworkResumed() -> NetStateHelper.sendStateToJs(it.eventCode,emitter)
            else -> {
              println("FrameworkEvent-----${it}")
            }
          }
        }
      }
      val networkChangeMonitor = ConnectivityStatusMonitor(GlobalScope)
      return NetworkChangeReceiver(networkChangeMonitor)
    }
  }
}
