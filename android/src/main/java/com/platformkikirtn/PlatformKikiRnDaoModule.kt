package com.platformkikirtn

import com.facebook.react.bridge.*
import com.kikitrade.dao.ResourceState
import com.kikitrade.framework.coroutine.PageScopeManager
import com.kikitrade.platform.container.ApplicationContainer
import com.kikitrade.platform.container.symbolRepository
import kotlinx.coroutines.launch

class PlatformKikiRnDaoModule(
  reactContext: ReactApplicationContext,
  private val applicationContainer: ApplicationContainer
) :
  ReactContextBaseJavaModule(reactContext) {

  override fun getName(): String {
    return NAME
  }


  @ReactMethod
  fun repositoryInvoke(
    scopeId: String,
    repositoryName: String,
    method: String,
    parameters: ReadableNativeMap,
    callback: Callback,
    promise: Promise
  ) {
    // this is a demo method for calling repository
    val pageScope = PageScopeManager.getOrCreatePageScope(scopeId)
    val symbolInfoRepository = applicationContainer.symbolRepository(pageScope)
    pageScope.launch {
      val refreshGetListResult = symbolInfoRepository.getList { db ->
        db.marketQueries.selectAllSymbols().executeAsList()
      }
      refreshGetListResult.mStateStorage.addObserver { pair ->
        when (val state = pair.second) {
          is ResourceState.Success -> {
            callback.invoke(state)
          }
          is ResourceState.Failed -> {
            if (!refreshGetListResult.hasLocalData) {
              // 初始状态下没有数据， 默认先返回的是loading， 这里要变更为error， 否则会一直loading
              promise.reject(state.error)
            }
          }
          else -> {
            //ignore
          }
        }
      }
      //TODO 需要包装为  ResourceState + value，   明确需要知道 loading， 还是 error 这里是简易demo
      promise.resolve(refreshGetListResult.mStateStorage.value)
    }
  }

  companion object {
    const val NAME = "PlatformKikiRnDao"
  }
}
