package com.platformkikirtn

import com.facebook.react.TurboReactPackage
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.NativeModule
import com.facebook.react.module.model.ReactModuleInfoProvider
import com.facebook.react.module.model.ReactModuleInfo
import com.kikitrade.platform.container.ApplicationContainer
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import java.util.HashMap

class PlatformKikiRtnPackage : TurboReactPackage() {

  private val mutex = Mutex()
  var application: ApplicationContainer? = null

  private fun createOrGetApplication(reactContext: ReactApplicationContext): ApplicationContainer {
    runBlocking {
      try {
        if (application != null) {
          return@runBlocking application!!
        }
        mutex.lock()
        application = ApplicationContainer(context = reactContext) {
          databaseVersion = 1
          databaseName = "kiki.db"
        }
      } finally {
        if(mutex.isLocked) mutex.unlock()
      }
    }
    return application!!
  }



  override fun getModule(name: String, reactContext: ReactApplicationContext): NativeModule? {
    val container = createOrGetApplication(reactContext)
    return when (name) {
        PlatformKikiRnCommonModule.NAME -> {
          PlatformKikiRnCommonModule(reactContext,container)
        }
        MarketModule.NAME -> {
          MarketModule(reactContext,container)
        }
        else -> {
          null
        }
    }
  }

  override fun getReactModuleInfoProvider(): ReactModuleInfoProvider {
    return ReactModuleInfoProvider {
      val moduleInfos: MutableMap<String, ReactModuleInfo> = HashMap()
      val isTurboModule: Boolean = BuildConfig.IS_NEW_ARCHITECTURE_ENABLED
      moduleInfos[PlatformKikiRnCommonModule.NAME] = ReactModuleInfo(
        PlatformKikiRnCommonModule.NAME,
        PlatformKikiRnCommonModule.NAME,
        false,  // canOverrideExistingModule
        false,  // needsEagerInit
        true,  // hasConstants
        false,  // isCxxModule
        isTurboModule // isTurboModule
      )
      moduleInfos[MarketModule.NAME] = ReactModuleInfo(
        MarketModule.NAME,
        MarketModule.NAME,
        false,
        false,
        true,
        false,
        isTurboModule
      )
      moduleInfos
    }
  }
}
