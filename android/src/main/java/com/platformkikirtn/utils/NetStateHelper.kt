package com.platformkikirtn.utils

import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableMap
import com.facebook.react.modules.core.DeviceEventManagerModule

/**
 * @author: louis.zhou@kikitrade.com
 * @date: 2023/4/3 16:58
 * @Description:
 */
class NetStateHelper {
  companion object {
    private const val NET_STATE_CHANGE_EVENT = "@netStateChangeEvent"
    private const val EVENT_UPDATE_KEY = "currentNetState"

    fun sendStateToJs(state:String,emitter: DeviceEventManagerModule.RCTDeviceEventEmitter?){
      println("FrameworkEvent----send${state}")
      val toJsData = Arguments.createMap()
      toJsData.putString(EVENT_UPDATE_KEY,state)
      emitter?.emit(NET_STATE_CHANGE_EVENT,state)
    }
  }
}
