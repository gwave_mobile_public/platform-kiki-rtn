package com.platformkikirtn.utils

import com.facebook.react.bridge.Arguments
import com.facebook.react.bridge.WritableArray
import com.facebook.react.bridge.WritableMap


//转换数据对象为WritableMap对象的方法 例如MarketData -> WritableMap
typealias ConvertToWritableFun<T> = (T) -> WritableMap

/**
 * 数据都封装一层统一格式再传递给js
 * 目前格式为： {data:WritableMap/WritableArray,isSuccess:Boolean}
 */
class ToJsHelper {
  companion object {
    private const val JS_DATA_KEY = "data"
    private const val DATA_SUCCESS_KEY = "isSuccess"
    // 用于远端请求更新数据
    private const val EVENT_UPDATE_KEY = "eventUpdateEventId"
    const val REMOTE_DATE_UPDATE_EVENT = "@remoteDataUpdateEvent"
    const val MARKET_CONNECT_TO_NATIVE_EVENT = "@marketConnectToNativeEvent"

    fun <T> convertToJsMap(data: T?, toWritableFun: ConvertToWritableFun<T>): WritableMap {
      val toJsData = Arguments.createMap()
      val dataWritableMap = data?.let { toWritableFun(it) }
      //TODO: 校验空值 js取到是什么
      toJsData.putMap(JS_DATA_KEY, dataWritableMap)
      toJsData.putBoolean(DATA_SUCCESS_KEY, true)
      return toJsData
    }

    fun <T> convertToJsArray(
      dataList: List<T>?,
      toWritableFun: ConvertToWritableFun<T>,
    ): WritableMap {
      val toJsData = Arguments.createMap()
      val dataWritableArray = Arguments.createArray()
      dataList?.forEach { dataWritableArray.pushMap(toWritableFun(it)) }
      toJsData.putArray(JS_DATA_KEY,dataWritableArray)
      toJsData.putBoolean(DATA_SUCCESS_KEY, true)
      return toJsData
    }

    fun makeErrorRespond():WritableMap{
      val toJsData = Arguments.createMap()
      toJsData.putBoolean(DATA_SUCCESS_KEY, false)
      return toJsData
    }

    fun <T> convertToEventUpdateJsMap(data: T?, eventId:String, toWritableFun: ConvertToWritableFun<T>): WritableMap {
      val toJsData = Arguments.createMap()
      val dataWritableMap = data?.let { toWritableFun(it) }
      toJsData.putMap(JS_DATA_KEY, dataWritableMap)
      toJsData.putBoolean(DATA_SUCCESS_KEY, true)
      toJsData.putString(EVENT_UPDATE_KEY, eventId)
      return toJsData
    }

    fun <T> convertToEventUpdateJsArray(dataList: List<T>?, eventId:String, toWritableFun: ConvertToWritableFun<T>): WritableMap {
      val toJsData = Arguments.createMap()
      val dataWritableArray = Arguments.createArray()
      dataList?.forEach { dataWritableArray.pushMap(toWritableFun(it)) }
      toJsData.putArray(JS_DATA_KEY, dataWritableArray)
      toJsData.putBoolean(DATA_SUCCESS_KEY, true)
      toJsData.putString(EVENT_UPDATE_KEY, eventId)
      return toJsData
    }

    fun <T> convertListDataToWritableArray(dataList: List<T>?,toWritableFun: ConvertToWritableFun<T>):WritableArray{
      val array = Arguments.createArray()
      dataList?.forEach { array.pushMap(toWritableFun(it)) }
      return array
    }
  }
}
