package com.platformkikirtn

import com.facebook.react.bridge.ReactApplicationContext

abstract class PlatformKikiRtnSpec internal constructor(context: ReactApplicationContext) :
  NativePlatformKikiRtnSpec(context) {
}
