package com.platformkikirtn

import com.facebook.react.bridge.*

/**
 * @author: louis.zhou@kikitrade.com
 * @date: 2023/2/9 11:04
 * @Description:
 */
abstract class MarketModuleSpec internal constructor(context: ReactApplicationContext) :
  ReactContextBaseJavaModule(context) {


  abstract fun preloadMarketLocalDB(promise: Promise)

  abstract fun getMarketData(scopeId: String, symbolNames: ReadableArray?, promise: Promise)

  abstract fun getMarketDataDetail(scopeId: String, symbolNames: ReadableArray?, promise: Promise)

  abstract fun connectMarketData(scopeId:String, symbolNames: ReadableArray?, promise: Promise)

  abstract fun connectMarketDataDetails(scopeId: String, symbolNames: ReadableArray?,promise: Promise)

  abstract fun getOfflineMarketDataDetail(scopeId:String,promise: Promise)

  abstract fun getMarketDetailTabZoneDataList(scopeId:String,num:Int,promise: Promise)

  abstract fun getMarketDetailByCondition(scopeId:String,baseCurrency: String, zoneKey: String, sortName: String?, sortType: String,promise: Promise)

  abstract fun getZoneListDetails(scopeId: String, maxRefreshTimeout: Double, promise: Promise)

  abstract fun getZoneDetail(
    scopeId: String,
    zoneKey:String,
    sortName: String,
    sortType: String,
    promise: Promise
  )

  abstract fun connectZoneDetail(
    scopeId: String,
    zoneKey:String,
    sortName: String,
    sortType: String,
    promise: Promise
  )

  abstract fun  getZoneDetailSymbols(
    scopeId: String,
    zoneKey: String,
    sortName: String,
    sortType: String,
    promise: Promise
  )

  abstract fun connectOrderBook(scopeId:String,promise: Promise)

  abstract fun connectDepth(scopeId:String,promise: Promise)

  abstract fun connectOrder(scopeId: String, promise: Promise)

  abstract fun sendMsgToWebsocket(scopeId:String,msg:ReadableMap)
}

