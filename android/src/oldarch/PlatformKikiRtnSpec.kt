package com.platformkikirtn

import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.*

abstract class PlatformKikiRtnSpec internal constructor(context: ReactApplicationContext) :
  ReactContextBaseJavaModule(context) {

  abstract fun getOrCreatePageScope(promise: Promise)

  abstract fun initApplicationEnv(type:String, params: ReadableMap)

  abstract fun updateJwtToken(jwtToken: String?)

  abstract fun updateInnerTest(status:Boolean)
  
  abstract fun multiply(a: Double, b: Double, promise: Promise);

}
