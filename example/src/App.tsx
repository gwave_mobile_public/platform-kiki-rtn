import * as React from 'react';

import { StyleSheet, View, Text, NativeModules } from 'react-native';
import { multiply, getOrCreatePageScope } from 'platform-kiki-rtn';
console.log(multiply, getOrCreatePageScope)

export default function App() {
  const [result, setResult] = React.useState<number | undefined>();
  const [pageScopeId, setPageScopeId] = React.useState<string | undefined>();


  React.useEffect(() => {
    multiply(3, 7).then(setResult);
  }, []);

  React.useEffect(() => {
    getOrCreatePageScope().then(setPageScopeId);
  }, []);

  return (
    <View style={styles.container}>
      <Text>Result: {result}</Text>
      <Text>PageScopeId: {pageScopeId}</Text>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
