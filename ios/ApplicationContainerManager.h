//
//  ApplicationContainerManager.h
//  PlatformKikiRtnExample
//
//  Created by 杨鹏 on 2023/2/16.
//

#import <Foundation/Foundation.h>
#import <KikiContainer/KikiContainer.h>

NS_ASSUME_NONNULL_BEGIN

#ifndef dispatch_main_async_safe
#define dispatch_main_async_safe(block)\
    if (dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL) == dispatch_queue_get_label(dispatch_get_main_queue())) {\
        block();\
    } else {\
        dispatch_async(dispatch_get_main_queue(), block);\
    }
#endif

@interface ApplicationContainerManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, strong, readonly) KikiContainerApplicationContainer * applicationContainer;

@end

NS_ASSUME_NONNULL_END
