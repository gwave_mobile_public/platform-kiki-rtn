//
//  ApplicationContainerManager.m
//  PlatformKikiRtnExample
//
//  Created by 杨鹏 on 2023/2/16.
//

#import "ApplicationContainerManager.h"
@interface ApplicationContainerManager()

@property (nonatomic, strong) KikiContainerApplicationContainer * applicationContainer;

@end

@implementation ApplicationContainerManager

+ (instancetype)sharedInstance
{
    static ApplicationContainerManager *shareInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[[self class] alloc] init];
    });
    return shareInstance;
}

- (KikiContainerApplicationContainer *)applicationContainer
{
    if (!_applicationContainer) {
        _applicationContainer = [[KikiContainerApplicationContainer alloc] initWithConfig:^(KikiContainerApplicationContainerConfiguration * _Nonnull configuration) {
            configuration.databaseName = @"kiki.db";
            configuration.databaseVersion = 1;
        }];
    }
    return _applicationContainer;
}

@end
