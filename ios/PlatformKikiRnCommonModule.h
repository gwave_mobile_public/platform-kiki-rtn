//
//  PlatformKikiRnCommonModule.h
//  PlatformKikiRtnExample
//
//  Created by 杨鹏 on 2023/2/16.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlatformKikiRnCommonModule : RCTEventEmitter<RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
