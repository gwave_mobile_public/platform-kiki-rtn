//
//  PlatformKikiRnCommonModule.m
//  PlatformKikiRtnExample
//
//  Created by 杨鹏 on 2023/2/16.
//

#import "PlatformKikiRnCommonModule.h"
#import "ApplicationContainerManager.h"
#import <KikiContainer/KikiContainer.h>

NSString *const NET_STATE_CHANGE_EVENT = @"@netStateChangeEvent";
NSString *const NET_STATE_UPDATE_KEY = @"currentNetState";

@interface PlatformKikiRnCommonModule()<KikiContainerCoroutineScope>

@end

@implementation PlatformKikiRnCommonModule

RCT_EXPORT_MODULE(PlatformKikiRnCommonModule)

- (instancetype)init
{
    if (self = [super init]) {
        [self setNotification];
    }
    return self;
}

- (void)setNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkchangenotification:) name:@"kNetworkReachabilityChangedNotification" object:nil];
    __weak typeof(self) weakSelf = self;
    [KikiContainerFrameworkContext.companion addListenerBlock:^(KikiContainerFrameworkEvent * _Nonnull event) {
        if ([event isKindOfClass:[ KikiContainerFrameworkEventNetworkDisconnected class]] || [event isKindOfClass:[ KikiContainerFrameworkEventNetworkUnstable class]] || [event isKindOfClass:[ KikiContainerFrameworkEventNetworkResumed class]] ){
            [weakSelf sendEventWithName:NET_STATE_CHANGE_EVENT body:@{NET_STATE_UPDATE_KEY:event.eventCode?:@""}];
        }
    }];
}

- (void)networkchangenotification:(NSNotification *)notification
{
    NSString *currentNetState = notification.userInfo[@"connectStatus"];
    KikiContainerConnectivityStatusMonitor *monitor = [[KikiContainerConnectivityStatusMonitor alloc] initWithCoroutine:self];
    KikiContainerNetworkChangeReceiver *receiver = [[KikiContainerNetworkChangeReceiver alloc] initWithMonitor:monitor];
    [receiver updateNetworkStatusConnectStatus:currentNetState.boolValue];
}

RCT_EXPORT_METHOD(getOrCreatePageScope:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *uuid = [[NSUUID UUID] UUIDString];
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:uuid];
        resolve(pageScope.id);
    });
}

RCT_EXPORT_METHOD(multiply:(double) a b:(double) b  resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        double result = a * b;
        
        resolve([NSString stringWithFormat:@"%.20lf", result]);
    });
}

RCT_EXPORT_METHOD(destroyPageScope:(NSString *)pageScopeId resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] findPageScopePageScopeId:pageScopeId];
        if (pageScope) {
            [pageScope cancelCause:nil];
        }
        resolve(@(YES));
    });
}

RCT_EXPORT_METHOD(initApplicationEnv:(NSString *)type params:(NSDictionary *)params){
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerEnvironmentType *envType;
          if ([type isEqualToString:@"dev"]) {
            envType = KikiContainerEnvironmentType.dev;
        } else if ([type isEqualToString:@"beta"]) {
            envType = KikiContainerEnvironmentType.beta;
        } else if ([type isEqualToString:@"prod"]) {
            envType = KikiContainerEnvironmentType.prod;
        } else if ([type isEqualToString:@"mock"]) {
            envType = KikiContainerEnvironmentType.mock;
        }else if([type isEqualToString:@"prodgreen"]){
            envType = KikiContainerEnvironmentType.prodGreen;
        }else {
            envType = KikiContainerEnvironmentType.dev;
        }
        
        NSString *jwtToken = [params objectForKey:@"jwtToken"];
        NSString *innerTest = [params objectForKey:@"innerTest"];
        NSString *deviceId = [params objectForKey:@"deviceId"];
        BOOL isInnerTest = innerTest.integerValue > 0 ? YES : NO;
        KikiContainerConstantConfig *constantConfig = [[KikiContainerConstantConfig alloc] initWithJwtToken:jwtToken?:@"" isInnerTest:isInnerTest deviceId:deviceId?:@""];
        [KikiContainerApplicationInitializer.companion initializeType:envType container:[ApplicationContainerManager sharedInstance].applicationContainer constantConfig:constantConfig];
    });
}

RCT_EXPORT_METHOD(updateJwtToken:(NSString *)jwtToken){
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerApplicationInitializer.companion.jwtToken = jwtToken;
    });
}

RCT_EXPORT_METHOD(updateInnerTest:(BOOL)status){
    dispatch_async(dispatch_get_main_queue(), ^{
        [[KikiContainerInnerTestManager shared] updateInnerTestState:status];
    });
}

- (NSArray<NSString *> *)supportedEvents
{
    return @[NET_STATE_CHANGE_EVENT];
}

@synthesize coroutineContext;

@end

