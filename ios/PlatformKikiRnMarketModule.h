//
//  PlatformKikiRnMarketModule.h
//  PlatformKikiRtnExample
//
//  Created by 杨鹏 on 2023/2/16.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <KikiContainer/KikiContainer.h>

typedef NS_ENUM(NSUInteger, PlatformKikiDBType) {
    PlatformKikiDBTickers = 0,
    PlatformKikiDBSymbols,
    PlatformKikiDBCurrencies,
    PlatformKikiDBZones
};

NS_ASSUME_NONNULL_BEGIN

typedef void (^FlowDataValueBlock)(id value);

@interface PlatformKikiRnMarketModule : RCTEventEmitter<RCTBridgeModule>

@end

@interface PlatformKikiDB : NSObject<KikiContainerKotlinSuspendFunction1>

@property (nonatomic) PlatformKikiDBType DBType;

@end

@interface PlatformKikiFlowData : NSObject<KikiContainerFlowCollector>

@property (nonatomic, copy) FlowDataValueBlock flowDataValueBlock;

@end

NS_ASSUME_NONNULL_END
