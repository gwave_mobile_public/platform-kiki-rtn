//
//  PlatformKikiRnMarketModule.m
//  PlatformKikiRtnExample
//
//  Created by 杨鹏 on 2023/2/16.
//

#import "PlatformKikiRnMarketModule.h"
#import "ApplicationContainerManager.h"
#import "ToJsHelper.h"

NSString *const REMOTE_DATE_UPDATE_EVENT = @"@remoteDataUpdateEvent";
NSString *const MARKET_CONNECT_TO_NATIVE_EVENT = @"@marketConnectToNativeEvent";

@interface PlatformKikiRnMarketModule()

@property (nonatomic, copy) NSString *tickersStr;
@property (nonatomic, copy) NSString *symbolsStr;
@property (nonatomic, copy) NSString *zonesStr;
@property (nonatomic, copy) NSString *currenciesStr;

@end

@implementation PlatformKikiRnMarketModule

RCT_EXPORT_MODULE(PlatformKikiRnMarketModule)

RCT_EXPORT_METHOD(connectMarketData:(NSString *)scopeId symbolNames:(NSArray *)symbolNames resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
        __weak typeof(self) weakSelf = self;
        KikiContainerConnectResult<NSArray<KikiContainerMarketData *> *> *connectResult = [[KikiContainerConnectResult alloc] initWithScope:pageScope afterClose:^{
            
        }];
        [marketDetailRepository connectSymbolNames:symbolNames result:connectResult completionHandler:^(NSError * _Nullable error) {
            NSString *eventId = connectResult.eventId;
            resolve(eventId);
            PlatformKikiFlowData *flowData = [[PlatformKikiFlowData alloc] init];
            [flowData setFlowDataValueBlock:^(id  _Nonnull value) {
                NSDictionary *jsonData = [ToJsHelper convertToEventUpdateDataList:value eventId:eventId];
                [weakSelf sendEventWithName:MARKET_CONNECT_TO_NATIVE_EVENT body:jsonData];
            }];
            dispatch_main_async_safe(^{
                [connectResult.flowData collectCollector:flowData completionHandler:^(NSError * _Nullable error) {
                    NSLog(@"flowData error: %@", error);
                }];
            })
        }];
    });
}

RCT_EXPORT_METHOD(getMarketData:(NSString *)scopeId symbolNames:(NSArray *)symbolNames resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            [marketDetailRepository getMarketDataSymbolNames:symbolNames state:KikiContainerSymbolStat.normal completionHandler:^(NSArray<KikiContainerMarketData *> * _Nullable_result dataList, NSError * _Nullable error) {
                NSDictionary *toJsDate = [ToJsHelper convertToJsArray:dataList];
                resolve(toJsDate);
            }];
        }
    });
}

RCT_EXPORT_METHOD(getOfflineMarketDataDetail:(NSString *)scopeId resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            NSString *eventId = [NSString stringWithFormat:@"EventUpdateId-%@",KikiContainerUUIDGeneratorKt.generateUUID];
            KikiContainerKotlinArray *statArray = [KikiContainerKotlinArray arrayWithSize:1 init:^id _Nullable(KikiContainerInt * _Nonnull containerIndex) {
                return KikiContainerSymbolStat.offline;
            }];
            [marketDetailRepository getLocalMarketDetailsByStateSymbolNames:nil state:statArray completionHandler:^(NSArray<KikiContainerMarketData *> * _Nullable_result localData, NSError * _Nullable error) {
                NSDictionary *toJsLocalData = [ToJsHelper convertToEventUpdateDataList:localData eventId:eventId];
                resolve(toJsLocalData);
            }];
           
            __weak typeof(self) weakSelf = self;
            
            [marketDetailRepository getRemoteMarketDetailsThenFilterByStateSymbolNames:nil states:statArray completionHandler:^(NSArray<KikiContainerMarketDataDetail *> * _Nullable remoteData, NSError * _Nullable error) {
                NSDictionary *toJsRemoteData = [ToJsHelper convertToEventUpdateDataList:remoteData eventId:eventId];
                [weakSelf sendEventWithName:REMOTE_DATE_UPDATE_EVENT body:toJsRemoteData];
            }];
        }
    });
}

RCT_EXPORT_METHOD(preloadMarketLocalDB:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *scopeId = [[NSUUID UUID] UUIDString];
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        KikiContainerTickerRepository *tickerRepository =[[ApplicationContainerManager sharedInstance].applicationContainer tickerRepositoryPageScope:pageScope];
        KikiContainerSymbolRepository *symbolRepository = [[ApplicationContainerManager sharedInstance].applicationContainer symbolRepositoryPageScope:pageScope];
        KikiContainerZoneRepository *zoneRepository = [[ApplicationContainerManager sharedInstance].applicationContainer zoneRepositoryPageScope:pageScope];
        KikiContainerCurrencyRepository *currencyRepository = [[ApplicationContainerManager sharedInstance].applicationContainer currencyRepositoryPageScope:pageScope];
        @try {
            if (!tickerRepository.checkIsDBPreload) {
                if (self.tickersStr.length > 0 && ![self.tickersStr isEqualToString:@"\n"]) {
                    [tickerRepository preloadTickersJson:self.tickersStr completionHandler:^(NSError * _Nullable error) {
                        NSLog(@"preloadTickersJson error = %@", error.localizedDescription);
                    }];
                } else {
                    PlatformKikiDB *ticketDB = [[PlatformKikiDB alloc] init];
                    ticketDB.DBType = PlatformKikiDBTickers;
                    [tickerRepository getListLocalDataQuery:ticketDB completionHandler:^(KikiContainerRefreshGetListResult<KikiContainerTicker *> * _Nullable listResult, NSError * _Nullable error) {
                        dispatch_main_async_safe(^{
                            [listResult awaitTimeout:1000 completionHandler:^(NSArray<id> * _Nullable dataList, NSError * _Nullable error) {

                            }];
                        });
                    }];
                }
            }
            if (!symbolRepository.checkIsDBPreload) {
                if (self.symbolsStr.length > 0 && ![self.symbolsStr isEqualToString:@"\n"]) {
                    [symbolRepository preloadSymbolsJson:self.symbolsStr completionHandler:^(NSError * _Nullable error) {
                        NSLog(@"preloadTickersJson error = %@", error.localizedDescription);
                    }];
                } else {
                    PlatformKikiDB *symbolDB = [[PlatformKikiDB alloc] init];
                    symbolDB.DBType = PlatformKikiDBSymbols;
                    [symbolRepository getListLocalDataQuery:symbolDB completionHandler:^(KikiContainerRefreshGetListResult<KikiContainerSymbol *> * _Nullable listResult, NSError * _Nullable error) {
                        dispatch_main_async_safe(^{
                            [listResult awaitTimeout:1000 completionHandler:^(NSArray<id> * _Nullable dataList, NSError * _Nullable error) {
                                
                            }];
                        });
                    }];
                }
            }
            
            if (!zoneRepository.checkIsDBPreload) {
                if (self.zonesStr.length > 0 && ![self.zonesStr isEqualToString:@"\n"]) {
                    [zoneRepository preloadZonesJson:self.zonesStr completionHandler:^(NSError * _Nullable error) {
                        NSLog(@"preloadZonesJson error = %@", error.localizedDescription);
                    }];
                } else {
                    PlatformKikiDB *zoneDB = [[PlatformKikiDB alloc] init];
                    zoneDB.DBType = PlatformKikiDBZones;
                    [zoneRepository getListLocalDataQuery:zoneDB completionHandler:^(KikiContainerRefreshGetListResult<KikiContainerSymbol *> * _Nullable listResult, NSError * _Nullable error) {
                        dispatch_main_async_safe(^{
                            [listResult awaitTimeout:1000 completionHandler:^(NSArray<id> * _Nullable dataList, NSError * _Nullable error) {
                                
                            }];
                        });
                    }];
                }
               
            }
            if (!currencyRepository.checkIsDBPreload) {
                if (self.currenciesStr.length > 0 && ![self.currenciesStr isEqualToString:@"\n"]) {
                    [currencyRepository preloadCurrenciesJson:self.currenciesStr completionHandler:^(NSError * _Nullable error) {
                        NSLog(@"preloadCurrenciesJson error = %@", error.localizedDescription);
                    }];
                } else {
                    PlatformKikiDB *currenciesDB = [[PlatformKikiDB alloc] init];
                    currenciesDB.DBType = PlatformKikiDBCurrencies;
                    [currencyRepository getListLocalDataQuery:currenciesDB completionHandler:^(KikiContainerRefreshGetListResult<KikiContainerSymbol *> * _Nullable listResult, NSError * _Nullable error) {
                        dispatch_main_async_safe(^{
                            [listResult awaitTimeout:1000 completionHandler:^(NSArray<id> * _Nullable dataList, NSError * _Nullable error) {
                                
                            }];
                        });
                    }];
                }
            }
            resolve(@(YES));
        } @catch (NSException *exception){
            resolve(@(NO));
            NSLog(@"preloadMarketLocalDB error:%@",exception.reason);
        }
    });
}

RCT_EXPORT_METHOD(connectMarketDataDetails:(NSString *)scopeId symbolNames:(NSArray *)symbolNames resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
        
        __weak typeof(self) weakSelf = self;
        KikiContainerConnectResult<NSArray<KikiContainerMarketDataDetail *> *> *connectResult = [[KikiContainerConnectResult alloc] initWithScope:pageScope afterClose:^{
            
        }];
        [marketDetailRepository connectDetailSymbolNames:symbolNames connectResul:connectResult completionHandler:^(NSError * _Nullable error) {
            NSString *eventId = connectResult.eventId;
            resolve(eventId);
            PlatformKikiFlowData *flowData = [[PlatformKikiFlowData alloc] init];
            [flowData setFlowDataValueBlock:^(id  _Nonnull value) {
                NSDictionary *jsonData = [ToJsHelper convertToEventUpdateDataList:value eventId:eventId];
                [weakSelf sendEventWithName:MARKET_CONNECT_TO_NATIVE_EVENT body:jsonData];
            }];
            dispatch_main_async_safe(^{
                [connectResult.flowData collectCollector:flowData completionHandler:^(NSError * _Nullable error) {
                    NSLog(@"flowData error: %@", error);
                }];
            })
        }];
    });
}

RCT_EXPORT_METHOD(getMarketDataDetail:(NSString *)scopeId symbolNames:(NSArray *)symbolNames resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            NSString *eventId = [NSString stringWithFormat:@"EventUpdateId-%@",KikiContainerUUIDGeneratorKt.generateUUID];
            KikiContainerKotlinArray *statArray = [KikiContainerKotlinArray arrayWithSize:3 init:^id _Nullable(KikiContainerInt * _Nonnull containerIndex) {
                if (containerIndex.intValue == 0) {
                    return KikiContainerSymbolStat.normal;
                } else if(containerIndex.intValue == 1) {
                    return KikiContainerSymbolStat.innerTest;
                }else{
                    return KikiContainerSymbolStat.explore;
                }
            }];
            [marketDetailRepository getLocalMarketDetailsByStateSymbolNames:symbolNames state:statArray completionHandler:^(NSArray<KikiContainerMarketData *> * _Nullable_result localData, NSError * _Nullable error) {
                NSDictionary *toJsLocalData = [ToJsHelper convertToEventUpdateDataList:localData eventId:eventId];
                resolve(toJsLocalData);
            }];
            
            __weak typeof(self) weakSelf = self;
            [marketDetailRepository getRemoteMarketDetailsThenFilterByStateSymbolNames:symbolNames states:statArray completionHandler:^(NSArray<KikiContainerMarketDataDetail *> * _Nullable remoteData, NSError * _Nullable error) {
                NSDictionary *toJsRemoteData =  [ToJsHelper convertToEventUpdateDataList:remoteData eventId:eventId];
                [weakSelf sendEventWithName:REMOTE_DATE_UPDATE_EVENT body:toJsRemoteData];
            }];
        }
    });
}

RCT_EXPORT_METHOD(getZoneListDetails:(NSString *)scopeId maxRefreshTimeout:(double)maxRefreshTimeout resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        KikiContainerZoneRepository *zoneRepository = [[ApplicationContainerManager sharedInstance].applicationContainer zoneRepositoryPageScope:pageScope];
        [zoneRepository getZoneListDetailsMaxRefreshTimeout:(int)maxRefreshTimeout completionHandler:^(NSArray<KikiContainerZoneBean *> * _Nullable_result resultList, NSError * _Nullable error) {
            if (resultList.count > 0) {
                resolve([ToJsHelper convertToJsArray:resultList]);
            } else {
                resolve([ToJsHelper makeErrorRespond]);
            }
        }];
    });
}

RCT_EXPORT_METHOD(getMarketDetailTabZoneDataList:(NSString *)scopeId num:(int)num resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerZoneRepository *zoneRepository = [[ApplicationContainerManager sharedInstance].applicationContainer zoneRepositoryPageScope:pageScope];
            [zoneRepository getMarketDetailTabZoneDataListNumber:num completionHandler:^(NSArray<KikiContainerZone *> * _Nullable_result dataList, NSError * _Nullable error) {
                NSDictionary *jsonData = [ToJsHelper convertToJsArray:dataList];
                resolve(jsonData);
            }];
        }
    });
}

RCT_EXPORT_METHOD(getMarketDetailByCondition:(NSString *)scopeId baseCurrency:(NSString *)baseCurrency zoneKey:(NSString *)zoneKey sortName:(NSString *)sortName sortType:(NSString *)sortType resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            NSString *eventId = [NSString stringWithFormat:@"EventUpdateId-%@",KikiContainerUUIDGeneratorKt.generateUUID];
            NSArray *localData =  [marketDetailRepository getLocalMarketDetailsByConditionBaseCurrency:baseCurrency zoneKey:zoneKey sortName:sortName sortType:sortType];
            NSDictionary *toJsLocalData = [ToJsHelper convertToEventUpdateDataList:localData eventId:eventId];
            resolve(toJsLocalData);
            __weak typeof(self) weakSelf = self;
            [marketDetailRepository getRemoteMarketDetailsByConditionBaseCurrency:baseCurrency zoneKey:zoneKey sortName:sortName sortType:sortType completionHandler:^(NSArray<KikiContainerMarketDataDetail *> * _Nullable_result dataList, NSError * _Nullable error) {
                NSDictionary *toRemoteData = [ToJsHelper convertToEventUpdateDataList:dataList eventId:eventId];
                [weakSelf sendEventWithName:REMOTE_DATE_UPDATE_EVENT body:toRemoteData];
            }];
        }
    });
}

RCT_EXPORT_METHOD(getZoneDetail:(NSString *)scopeId zoneKey:(NSString *)zoneKey sortName:(NSString *)sortName sortType:(NSString *)sortType resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerZoneRepository *zoneRepositiory = [[ApplicationContainerManager sharedInstance].applicationContainer zoneRepositoryPageScope:pageScope];
            [zoneRepositiory getZoneDetailKey:zoneKey sortName:sortName sortType:sortType completionHandler:^(KikiContainerZoneDetail * _Nullable localData, NSError * _Nullable error) {
                NSDictionary *result = [ToJsHelper convertToJsDict:localData];
                resolve(result);
            }];
        }
    });
}

RCT_EXPORT_METHOD(getZoneDetailSymbols:(NSString *)scopeId zoneKey:(NSString *)zoneKey sortName:(NSString *)sortName sortType:(NSString *)sortType resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerZoneRepository *zoneRepositiory = [[ApplicationContainerManager sharedInstance].applicationContainer zoneRepositoryPageScope:pageScope];
            [zoneRepositiory getZoneDetailKey:zoneKey sortName:sortName sortType:sortType completionHandler:^(KikiContainerZoneDetail * _Nullable zoneDetail, NSError * _Nullable error) {
                NSDictionary *toJsData = [ToJsHelper convertToJsArray:zoneDetail.symbolList];
                resolve(toJsData);
            }];
        }
    });
}

RCT_EXPORT_METHOD(connectZoneDetail:(NSString *)scopeId zoneKey:(NSString *)zoneKey sortName:(NSString *)sortName sortType:(NSString *)sortType resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            KikiContainerZoneRepository *zoneRepositiory = [[ApplicationContainerManager sharedInstance].applicationContainer zoneRepositoryPageScope:pageScope];
            
            __weak typeof(self) weakSelf = self;
            [zoneRepositiory getZoneDetailKey:zoneKey sortName:sortName sortType:sortType completionHandler:^(KikiContainerZoneDetail * _Nullable localData, NSError * _Nullable error) {
                // step 1
                [marketDetailRepository getSymbolsList:localData.symbolList completionHandler:^(NSArray<NSString *> * _Nullable symbols, NSError * _Nullable error) {
                    // step 2
                    [zoneRepositiory getZoneListMapList:localData.symbolList completionHandler:^(KikiContainerMutableDictionary<NSString *,KikiContainerMarketData *> * _Nullable_result map, NSError * _Nullable error) {
                        KikiContainerConnectResult<NSArray<KikiContainerMarketDataDetail *> *> *connectResult = [[KikiContainerConnectResult alloc] initWithScope:pageScope afterClose:^{
                            
                        }];
                        [marketDetailRepository connectDetailSymbolNames:symbols connectResul:connectResult completionHandler:^(NSError * _Nullable error) {
                            NSString *eventId = connectResult.eventId;
                            resolve(eventId);

                            PlatformKikiFlowData *flowData = [[PlatformKikiFlowData alloc] init];
                            [flowData setFlowDataValueBlock:^(id  _Nonnull value) {
                                NSArray *valueArray = value;
                                [valueArray enumerateObjectsUsingBlock:^(KikiContainerMarketDataDetail  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    if ([[map allKeys] containsObject:obj.currency]) {
                                        map[obj.currency] = obj;
                                    }
                                }];
                                KikiContainerZoneHeader *header = [zoneRepositiory exchangeZoneDetailHeaderList:map.allValues];
                                KikiContainerZoneDetail *zoneDetail = [[KikiContainerZoneDetail alloc] initWithHeader:header symbolList:map.allValues];
                                NSDictionary *dynamic = [ToJsHelper convertToEventUpdateData:zoneDetail eventId:eventId];
                                [weakSelf sendEventWithName:MARKET_CONNECT_TO_NATIVE_EVENT body:dynamic];
                            }];
                            dispatch_main_async_safe(^{
                                [connectResult.flowData collectCollector:flowData completionHandler:^(NSError * _Nullable error) {
                                }];
                            });
                        }];
                    }];
                }];
            }];
        }
    });
}

RCT_EXPORT_METHOD(connectOrderBook:(NSString *)scopeId resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];

            __weak typeof(self) weakSelf = self;
            KikiContainerConnectResult<KikiContainerOrderBookWs *> *connectResult = [[KikiContainerConnectResult alloc] initWithScope:pageScope afterClose:^{
                
            }];
            [marketDetailRepository connectOrderBookResult:connectResult completionHandler:^(NSError * _Nullable error) {
                NSString *eventId = connectResult.eventId;
                resolve(eventId);

                // 事件接受
                PlatformKikiFlowData *flowData = [[PlatformKikiFlowData alloc] init];
                [flowData setFlowDataValueBlock:^(id  _Nonnull value) {
                    NSDictionary *dynamic = [ToJsHelper convertToEventUpdateData:value eventId:eventId];
                    [weakSelf sendEventWithName:MARKET_CONNECT_TO_NATIVE_EVENT body:dynamic];
                }];
                dispatch_main_async_safe(^{
                    [connectResult.flowData collectCollector:flowData completionHandler:^(NSError * _Nullable error) {
                    }];
                });
            }];
        }
    });
}

RCT_EXPORT_METHOD(connectDepth:(NSString *)scopeId resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            KikiContainerConnectResult<KikiContainerDepthWs *> *connectResult = [[KikiContainerConnectResult alloc] initWithScope:pageScope afterClose:^{
                
            }];
            __weak typeof(self) weakSelf = self;
            [marketDetailRepository connectDepthResult:connectResult completionHandler:^(NSError * _Nullable error) {
                NSString *eventId = connectResult.eventId;
                resolve(eventId);

                // 事件接收
                PlatformKikiFlowData *flowData = [[PlatformKikiFlowData alloc] init];
                [flowData setFlowDataValueBlock:^(id  _Nonnull value) {
                    NSDictionary *dynamic = [ToJsHelper convertToEventUpdateData:value eventId:eventId];
                    [weakSelf sendEventWithName:MARKET_CONNECT_TO_NATIVE_EVENT body:dynamic];
                }];
                dispatch_main_async_safe(^{
                    [connectResult.flowData collectCollector:flowData completionHandler:^(NSError * _Nullable error) {
                    }];
                });
            }];
        }
    });
}

RCT_EXPORT_METHOD(connectOrder:(NSString *)scopeId resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketDetailRepository *marketDetailRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketDetailRepositoryPageScope:pageScope];
            KikiContainerConnectResult<KikiContainerOrderWs *> *connectResult = [[KikiContainerConnectResult alloc] initWithScope:pageScope afterClose:^{
                
            }];
            __weak typeof(self) weakSelf = self;
            [marketDetailRepository connectOrderResult:connectResult completionHandler:^(NSError * _Nullable error) {
                NSString *eventId = connectResult.eventId;
                resolve(eventId);

                // 事件接收
                PlatformKikiFlowData *flowData = [[PlatformKikiFlowData alloc] init];
                [flowData setFlowDataValueBlock:^(id  _Nonnull value) {
                    NSDictionary *dynamic = [ToJsHelper convertToEventUpdateData:value eventId:eventId];
                    [weakSelf sendEventWithName:MARKET_CONNECT_TO_NATIVE_EVENT body:dynamic];
                }];
                dispatch_main_async_safe(^{
                    [connectResult.flowData collectCollector:flowData completionHandler:^(NSError * _Nullable error) {
                    }];
                });
            }];
        }
    });
}

RCT_EXPORT_METHOD(sendMsgToWebsocket:(NSString *)scopeId ssg:(NSDictionary *)msg resolve:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
    dispatch_async(dispatch_get_main_queue(), ^{
        KikiContainerPageScope *pageScope = [[KikiContainerPageScopeManager shared] getOrCreatePageScopePageScopeId:scopeId];
        if (pageScope) {
            KikiContainerMarketSocketRepository *marketSocketRepository = [[ApplicationContainerManager sharedInstance].applicationContainer marketSocketRepository];

            // 保证body里value都是string类型
            NSMutableDictionary *body = [NSMutableDictionary dictionary];
            [msg enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                [body setObject:[obj stringValue] forKey:key];
            }];
            
            [marketSocketRepository sendRoomMsgMsgInfo:body completionHandler:^(NSError * _Nullable error) {
                  // 发送数据完毕
            }];
        }
    });
}


- (NSArray<NSString *> *)supportedEvents
{
    return @[MARKET_CONNECT_TO_NATIVE_EVENT, REMOTE_DATE_UPDATE_EVENT];
}

- (NSString *)tickersStr
{
    if (!_tickersStr) {
        NSString *tickersPath = [[NSBundle mainBundle] pathForResource:@"tickers" ofType:@"json"];
        _tickersStr = [NSString stringWithContentsOfFile:tickersPath encoding:NSUTF8StringEncoding error:nil];
    }
    return _tickersStr;
}

- (NSString *)symbolsStr
{
    if (!_symbolsStr) {
        NSString *symbolsPath = [[NSBundle mainBundle] pathForResource:@"symbols" ofType:@"json"];
        _symbolsStr = [NSString stringWithContentsOfFile:symbolsPath encoding:NSUTF8StringEncoding error:nil];
    }
    return _symbolsStr;
}

- (NSString *)zonesStr
{
    if (!_zonesStr) {
        NSString *zonesPath = [[NSBundle mainBundle] pathForResource:@"zones" ofType:@"json"];
        _zonesStr = [NSString stringWithContentsOfFile:zonesPath encoding:NSUTF8StringEncoding error:nil];
    }
    return _zonesStr;
}

- (NSString *)currenciesStr
{
    if (!_currenciesStr) {
        NSString *currenciesPath = [[NSBundle mainBundle] pathForResource:@"currencies" ofType:@"json"];
        _currenciesStr = [NSString stringWithContentsOfFile:currenciesPath encoding:NSUTF8StringEncoding error:nil];
    }
    return _currenciesStr;
}


@end


@implementation PlatformKikiDB

- (void)invokeP1:(id _Nullable)p1 completionHandler:(nonnull void (^)(id _Nullable_result, NSError * _Nullable))completionHandler {
    id<KikiContainerMarket>market = p1;
    if (self.DBType == PlatformKikiDBTickers) {
        completionHandler([market.marketQueries.selectAllTickers executeAsList], nil);
    } else if (self.DBType == PlatformKikiDBSymbols) {
        completionHandler([market.marketQueries.selectAllSymbols executeAsList], nil);
    } else if (self.DBType == PlatformKikiDBCurrencies) {
        completionHandler([market.marketQueries.selectAllCurrencies executeAsList], nil);
    } else {
        completionHandler([market.marketQueries.selectAllZones executeAsList], nil);
    }
}

@end

@implementation PlatformKikiFlowData

- (void)emitValue:(id _Nullable)value completionHandler:(nonnull void (^)(NSError * _Nullable))completionHandler {
    if(self.flowDataValueBlock) {
        self.flowDataValueBlock(value);
    }
    completionHandler(nil);
}

@end

