
#ifdef RCT_NEW_ARCH_ENABLED
#import "RNPlatformKikiRtnSpec.h"

@interface PlatformKikiRtn : NSObject <NativePlatformKikiRtnSpec>
#else
#import <React/RCTBridgeModule.h>

@interface PlatformKikiRtn : NSObject <RCTBridgeModule>
#endif

@end
