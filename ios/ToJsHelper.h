//
//  ToJsHelper.h
//  platform-kiki-rtn
//
//  Created by 杨鹏 on 2023/2/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ToJsHelper : NSObject

+ (NSDictionary *)convertToJsDict:(id)dataDict;

+ (NSDictionary *)convertToJsArray:(NSArray *)dataArray;

+ (NSDictionary *)makeErrorRespond;

+ (NSDictionary *)convertToEventUpdateData:(id)dataDict eventId:(NSString *)eventId;

+ (NSDictionary *)convertToEventUpdateDataList:(NSArray *)dataList eventId:(NSString *)eventId;

@end

NS_ASSUME_NONNULL_END
