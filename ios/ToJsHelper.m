//
//  ToJsHelper.m
//  platform-kiki-rtn
//
//  Created by 杨鹏 on 2023/2/17.
//

#import "ToJsHelper.h"
#import <KikiContainer/KikiContainer.h>

NSString *const JS_DATA_KEY = @"data";
NSString *const DATA_SUCCESS_KEY = @"isSuccess";
NSString *const EVENT_UPDATE_KEY = @"eventUpdateEventId";

@implementation ToJsHelper

+ (NSDictionary *)convertToJsDict:(id)dataDict
{
    NSMutableDictionary *toJsData = [[NSMutableDictionary alloc] init];
        
    NSDictionary *dict;
    if ([dataDict isKindOfClass:[KikiContainerZoneDetail class]]) {
        dict = [self formatZoneDetail:dataDict];
    } else if ([dataDict isKindOfClass:[KikiContainerZoneHeader class]]) {
        dict = [self formatZoneHeader:dataDict];
    } else {
        dict = [self formatMarketData:dataDict];
    }
    if (dict) {
        [toJsData setValue:dict forKey:JS_DATA_KEY];
    }
    [toJsData setValue:@(YES) forKey:DATA_SUCCESS_KEY];
    return toJsData.copy;
}

+ (NSDictionary *)convertToJsArray:(NSArray *)dataArray
{
    NSMutableDictionary *toJsData = [[NSMutableDictionary alloc] init];
    if (dataArray) {
        NSMutableArray *toJsArray = [[NSMutableArray alloc] init];
        [dataArray enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dict = [[NSDictionary alloc] init];
            if([obj isKindOfClass:[KikiContainerZoneBean class]]){
                dict = [self formatZonebeanData:obj];
            } else if ([obj isKindOfClass:[KikiContainerZone class]]) {
                dict = [self formatMarketTabInfo:obj];
            } else {
                dict = [self formatMarketData:obj];
            }
            [toJsArray addObject:dict];
        }];
        [toJsData setValue:toJsArray forKey:JS_DATA_KEY];
    }
    [toJsData setValue:@(YES) forKey:DATA_SUCCESS_KEY];
    return toJsData.copy;
}

+ (NSDictionary *)makeErrorRespond
{
    NSMutableDictionary *toJsData = [[NSMutableDictionary alloc] init];
    [toJsData setValue:@(NO) forKey:DATA_SUCCESS_KEY];
    return toJsData.copy;
}

+ (NSMutableDictionary *)formatMarketData:(KikiContainerMarketData *)data
{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    NSInteger symbolPrecision = data.symbolPrecision;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:symbolPrecision];
    [formatter setMinimumFractionDigits:symbolPrecision];
    
    [dataDict setObject:data.coinCode forKey:@"symbol"];
    [dataDict setObject:data.coinCode forKey:@"coinCode"];
    [dataDict setObject:[formatter stringFromNumber:[NSNumber numberWithDouble:data.priceLast]] forKey:@"priceLast"];
    [dataDict setObject:[NSString stringWithFormat:@"%.2f",data.volume] forKey:@"volume"];
    [dataDict setObject:[NSString stringWithFormat:@"%.2f",data.quoteVolume] forKey:@"quoteVolume"];
    [dataDict setObject:[NSString stringWithFormat:@"%.2f",data.riseAndFall] forKey:@"riseAndFall"];
    [dataDict setObject:[formatter stringFromNumber:[NSNumber numberWithDouble:data.highPrice]] forKey:@"highPrice"];
    [dataDict setObject:[formatter stringFromNumber:[NSNumber numberWithDouble:data.lowPrice]] forKey:@"lowPrice"];
    [dataDict setObject:@(data.baseVolume) forKey:@"baseVolume"];
    [dataDict setObject:@(data.priceLastRise) forKey:@"priceLastRise"];
    [dataDict setObject:@(data.ask) forKey:@"ask"];
    [dataDict setObject:@(data.bid) forKey:@"bid"];
    [dataDict setObject:data.currency forKey:@"currency"];
    [dataDict setObject:data.quoteCurrency forKey:@"quoteCurrency"];
    [dataDict setObject:@(data.modified) forKey:@"modified"];
    [dataDict setObject:@(data.symbolPrecision) forKey:@"precision"];
    [dataDict setObject:@(data.symbolPrecision) forKey:@"symbolPrecision"];
    [dataDict setObject:@(data.volumePrecision) forKey:@"volumePrecision"];
    [dataDict setObject:@(data.tradePrecision) forKey:@"tradePrecision"];
    [dataDict setObject:@(data.tradeInputPrecision) forKey:@"tradeInputPrecision"];
    [dataDict setObject:@(data.tradeVolumePrecision) forKey:@"tradeVolumePrecision"];
    [dataDict setObject:@(data.marketFreezeBuffer) forKey:@"marketFreezeBuffer"];
    [dataDict setObject:@(data.sort) forKey:@"sort"];
    [dataDict setObject:data.baseCurrency forKey:@"baseCurrencyKey"];
    [dataDict setObject:@(data.orderMax) forKey:@"orderMax"];
    [dataDict setObject:@(data.orderMin) forKey:@"orderMin"];
    [dataDict setObject:@(data.allowTrade) forKey:@"allowTrade"];
    [dataDict setObject:@(data.state) forKey:@"state"];

    if([data isKindOfClass:[KikiContainerMarketDataDetail class]]) {
        KikiContainerMarketDataDetail *dataDetail = (KikiContainerMarketDataDetail *)data;
        [dataDict setObject:@(dataDetail.orderBaseMax) forKey:@"orderBaseMax"];
        [dataDict setObject:@(dataDetail.orderBaseMin) forKey:@"orderBaseMin"];
        [dataDict setObject:@(dataDetail.orderQuoteMax) forKey:@"orderQuoteMax"];
        [dataDict setObject:@(dataDetail.orderQuoteMin) forKey:@"orderQuoteMin"];
    }
    
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    [data.priceList enumerateObjectsUsingBlock:^(KikiContainerDouble * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [dataArray addObject:obj];
    }];
    [dataDict setObject:dataArray forKey:@"priceList"];
    return dataDict;
}

+ (NSMutableDictionary *)formatZonebeanData:(KikiContainerZoneBean *)zoneBean
{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setObject:zoneBean.key forKey:@"key"];
    [dataDict setObject:zoneBean.name forKey:@"name"];
    [dataDict setObject:zoneBean.leadingCurrency?:@"" forKey:@"leadingCurrency"];
    [dataDict setObject:@(zoneBean.order) forKey:@"order"];
    [dataDict setObject:@(zoneBean.status) forKey:@"status"];
    [dataDict setObject:zoneBean.fallCount?:@(0) forKey:@"fallCount"];
    [dataDict setObject:zoneBean.riseCount?:@(0) forKey:@"riseCount"];
    [dataDict setObject:zoneBean.percent?:@(0.0) forKey:@"percent"];
    [dataDict setObject:zoneBean.leadingPercent?:@(0.0) forKey:@"leadingPercent"];
    [dataDict setObject:zoneBean.picPaths?:@[] forKey:@"picPaths"];
    [dataDict setObject:zoneBean.bars?:@[] forKey:@"bars"];
    return dataDict;
}

+ (NSMutableDictionary *)formatMarketTabInfo:(KikiContainerZone *)zone {
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setObject:zone.name forKey:@"name"];
    [dataDict setObject:zone.key forKey:@"key"];
    return dataDict;
}

+ (NSDictionary *)convertToEventUpdateData:(id)dataDict eventId:(NSString *)eventId {
    NSMutableDictionary *toJsData = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if ([dataDict isKindOfClass:[KikiContainerOrderBookWs class]]) {
        dict = [self formatOrderBookSocket:dataDict];
    } else if ([dataDict isKindOfClass:[KikiContainerDepthWs class]]) {
        dict = [self formatOrderBookDepthSocket: dataDict];
    } else if ([dataDict isKindOfClass:[KikiContainerZoneDetail class]]) {
        dict = [self formatZoneDetail: dataDict];
    }else if([dataDict isKindOfClass:[KikiContainerOrderWs class]]){
        dict = [self formatOrder: dataDict];
    }else {
        dict = [self formatMarketData:dataDict];
    }
    if (dict) {
        [toJsData setValue:dict forKey:JS_DATA_KEY];
    }
    [toJsData setValue:@(YES) forKey:DATA_SUCCESS_KEY];
    [toJsData setValue:eventId forKey:EVENT_UPDATE_KEY];
    return toJsData.copy;
}

+ (NSDictionary *)convertToEventUpdateDataList:(NSArray *)dataArray eventId:(NSString *)eventId {
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    if (dataArray) {
        NSMutableArray *toJsArray = [[NSMutableArray alloc] init];
        [dataArray enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dict = [[NSDictionary alloc] init];
            dict = [self formatMarketData:obj];
            [toJsArray addObject:dict];
        }];
        [dataDict setValue:toJsArray forKey:JS_DATA_KEY];
    }
    [dataDict setValue:@(YES) forKey:DATA_SUCCESS_KEY];
    [dataDict setValue:eventId forKey:EVENT_UPDATE_KEY];
    return dataDict;
}

+ (NSMutableDictionary *)formatZoneDetail:(id)dataDict
{
    KikiContainerZoneDetail *zoneDetail = (KikiContainerZoneDetail *)dataDict;
    NSDictionary *zoneHeader = [ToJsHelper formatZoneHeader:zoneDetail.header];
    NSMutableDictionary *zoneDict = [[NSMutableDictionary alloc] init];
    NSArray *symbolArray = [ToJsHelper convertListData:zoneDetail.symbolList];
    [zoneDict setObject:symbolArray?:@[] forKey:@"symbolList"];
    [zoneDict setObject:zoneHeader forKey:@"header"];
    return zoneDict;
}

+ (NSDictionary *)formatZoneHeader:(id)dataDict
{
    KikiContainerZoneHeader *header = (KikiContainerZoneHeader *)dataDict;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@(header.allRiseAndFall) forKey:@"allRiseAndFall"];
    [dict setObject:header.riseCount?:0 forKey:@"riseCount"];
    [dict setObject:header.fallCount?:0 forKey:@"fallCount"];
    [dict setObject:header.leadingCurrency?:@"" forKey:@"leadingCurrency"];
    [dict setObject:header.leadingCurrencyRise?:@(0.0) forKey:@"leadingCurrencyRise"];
    return dict;
}

+ (NSMutableDictionary *)formatOrder:(id)dataDict
{
    KikiContainerOrderWs *order = (KikiContainerOrderWs *)dataDict;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@(order.averageTradePrice) forKey:@"averageTradePrice"];
    [dict setObject:order.baseAccountId forKey:@"baseAccountId"];
    [dict setObject:order.baseCurrency forKey:@"baseCurrency"];
    [dict setObject:@(order.baseQty) forKey:@"baseQty"];
    [dict setObject:order.businessType forKey:@"businessType"];
    
    [dict setObject:@(order.created) forKey:@"created"];
    [dict setObject:@(order.fee) forKey:@"fee"];
    [dict setObject:order.feeCurrency forKey:@"feeCurrency"];
    [dict setObject:@(order.filledQty) forKey:@"filledQty"];
    [dict setObject:@(order.filledVolume) forKey:@"filledVolume"];
    
    [dict setObject:@(order.latestVersion) forKey:@"latestVersion"];
    [dict setObject:@(order.migration) forKey:@"migration"];
    [dict setObject:@(order.modified) forKey:@"modified"];
    [dict setObject:order.orderBookId forKey:@"orderBookId"];
    [dict setObject:order.orderCancelStatus forKey:@"orderCancelStatus"];
    [dict setObject:order.orderId forKey:@"orderId"];
    [dict setObject:order.orderStatus forKey:@"orderStatus"];
    [dict setObject:order.orderType forKey:@"orderType"];
    
    [dict setObject:@(order.price) forKey:@"price"];
    [dict setObject:order.quoteAccountId forKey:@"quoteAccountId"];
    [dict setObject:order.quoteCurrency forKey:@"quoteCurrency"];
    [dict setObject:@(order.quoteQty) forKey:@"quoteQty"];
    
    [dict setObject:@(order.remainingQty) forKey:@"remainingQty"];
    [dict setObject:@(order.remainingVolume) forKey:@"remainingVolume"];
    [dict setObject:order.saasId forKey:@"saasId"];
    [dict setObject:order.side forKey:@"side"];
    [dict setObject:order.symbol forKey:@"symbol"];
    [dict setObject:order.takenProcessId forKey:@"takenProcessId"];
    [dict setObject:order.type forKey:@"type"];
    [dict setObject:@(order.updateAveragePrice) forKey:@"updateAveragePrice"];
    [dict setObject:@(order.updateFee) forKey:@"updateFee"];
    [dict setObject:@(order.updateOrderBookId) forKey:@"updateOrderBookId"];
    [dict setObject:order.precisions forKey:@"precisions"];

    return dict;
}

+ (NSArray *)convertListData:(NSArray *)datalist
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    if (datalist.count > 0) {
        [datalist enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *dict = [self formatMarketData:obj];
            [dataArray addObject:dict];
        }];
    }
    return dataArray.copy;
}

+ (NSMutableDictionary *)formatOrderBookSocket:(id)dataDict {
    KikiContainerOrderBookWs *orderBookWs = (KikiContainerOrderBookWs *)dataDict;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:orderBookWs.channel forKey:@"channel"];
    [dict setObject:@(orderBookWs.level) forKey:@"level"];
    [dict setObject:orderBookWs.symbol forKey:@"symbol"];
    [dict setObject:orderBookWs.type forKey:@"type"];
    [dict setObject:orderBookWs.values ?: @"" forKey:@"values"];
    return dict;
}

+ (NSMutableDictionary *)formatOrderBookDepthSocket:(id)dataDict {
    KikiContainerDepthWs *depthWs = (KikiContainerDepthWs *)dataDict;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:depthWs.channel forKey:@"channel"];
    [dict setObject:depthWs.symbol forKey:@"symbol"];
    [dict setObject:depthWs.type forKey:@"type"];
    [dict setObject:depthWs.values ?: @"" forKey:@"values"];
    return dict;
}


@end
