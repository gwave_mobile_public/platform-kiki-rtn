
import { MarketDeviceEventEmitter } from '../../SocketModule';
import JsSdkEventEmit from '../../utils/JsSdkEventEmit'

const RemoteEventEmit = new JsSdkEventEmit()


const REMOTE_DATE_UPDATE_EVENT = "@remoteDataUpdateEvent"

class RemoteLisManager {
    
    addParentEventLis = () => {
        return MarketDeviceEventEmitter.addListener(REMOTE_DATE_UPDATE_EVENT, (remoteData: { eventUpdateEventId: string, data: any, isSuccess: boolean }) => {
            const { eventUpdateEventId } = remoteData
            // 进行转义，底层说  看以后是否将  
            RemoteEventEmit.emitEvent(eventUpdateEventId, { data: remoteData.data, isSuccess: remoteData.isSuccess })
        })
    }

    addChildEventLis = (eventUpdateEventId: string, callBack: (eventData: any) => void) => {
        if (!!eventUpdateEventId) {
            const removeCallBack = RemoteEventEmit.addEventListener(eventUpdateEventId, callBack)
            return removeCallBack
        }
        return null
    }

    removeAllRemoteHttpLis = () => {
        RemoteEventEmit.removeAllListener()
    }
}


export default new RemoteLisManager()