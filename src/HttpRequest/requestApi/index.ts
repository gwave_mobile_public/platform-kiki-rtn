
import type { EventDataType, EventDataTypeWithLis } from '../../NativeBridgeModule'
import RemoteLisManager from '../RemoteLisManager'


type RemotePushOptions<T> = (data: T) => void

type ListenerReturnType<T> = (remoteDataNotifyCallBack: RemotePushOptions<T>) => (() => void) | null


export type ResponentDataType<T> = {
    data: T,
    isSuccess?: boolean
    addRemoteListener?: (listener: (data: EventDataType<T>) => void) => ()=>void,
}

class RtnResponence {

    addRemoteListenerHoc = <T>(eventUpdateEventId: string) => {
        return (remoteDataNotifyCallBack: RemotePushOptions<T>) => {
            const removeCallBack = RemoteLisManager.addChildEventLis(eventUpdateEventId, remoteDataNotifyCallBack)
            return removeCallBack
        }
    }

    transformResult = <T>(result: EventDataTypeWithLis<T>, addRemoteListener?: ListenerReturnType<T>) => {
        if (result?.eventUpdateEventId) {
            return {
                // eventUpdateEventId: result.eventUpdateEventId,
                data: result.data,
                isSuccess: result.isSuccess,
                addRemoteListener
            } as ResponentDataType<T>
        }

        return result as ResponentDataType<T>
    }
}

export const RtnResponenceInstance = new RtnResponence()