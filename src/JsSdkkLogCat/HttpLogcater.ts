import LogcatConfig from './LogcatConfig'


class HttpLogcater {
    constructor() {

    }
    
    /**
     * 测试人员抓日志
     * @param args 
     */
    httpLogcaterTest(...args: any[]) {
        LogcatConfig.logcat('HttpLogcater:', 'httpLogcaterTest', ...args)
    }

    /**
     * 开发人员调试
     * @param args 
     */
    httpLogcaterDev(...args: any[]) {
        LogcatConfig.logcat('HttpLogcater:', 'httpLogcaterDev', ...args)
    }


}

export default new HttpLogcater()