
import { JsSdkConfig } from "../SdkInitApi";
import isDev from "../utils/isDev";

type LogFuncType = (message: string, ...otherLog: string[]) => void

class LogcatConfig {
    protected isOpen: boolean;
    protected logFunc: LogFuncType;
    constructor() {
        this.isOpen = false;
        this.logFunc = () => {

        }
    }

    setLogFunc(logFunc: LogFuncType, isOpen: boolean) {
        if (isOpen) {
            if (isDev) {
                this.logFunc = logFunc
            }
            if (!isDev && !JsSdkConfig.isProdProject()) {// release 包并且测试包 
                this.logFunc = logFunc
            }
        }
    }

    logcat(logFileName: string, message: string, ...args: any[]) {
        this.logFunc(logFileName, message, ...args)
    }

    
}

export default new LogcatConfig()