import LogcatConfig from './LogcatConfig'


class SocketLogcater {
    constructor() {

    }
    
    /**
     * 测试人员抓日志
     * @param args 
     */
    socketLogcatManagerTest(...args: any[]) {
        LogcatConfig.logcat('SocketLogcater:', 'socketLogcatManagerTest', ...args)
    } 

    socketLogcatManagerPush(...args: any[]) {
        LogcatConfig.logcat('SocketLogcater:', 'socketLogcatManagerPush', ...args)
    }

    /**
     * 开发人员调试
     * @param args 
     */
    socketLogcatManagerDev(...args: any[]) {
        LogcatConfig.logcat('SocketLogcater:', 'socketLogcatManagerDev', ...args)
    }


}

export default new SocketLogcater()