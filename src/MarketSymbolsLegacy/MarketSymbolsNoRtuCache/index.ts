import isEmpty from 'lodash/isEmpty'
import differenceBy from 'lodash/differenceBy'

import { action, flow, makeObservable, observable } from 'mobx';
import { MarketSymbolsCache } from '../../SocketModule';
import { HttpLogcater } from '../../JsSdkkLogCat';
import { JsSdkEventEmitInstance } from '../../utils/JsSdkEventEmit'
import {rtnClientInstance} from '@kikitrade/rtncodegen'

import type { ExtractMarketData, EventDataTypeWithLis, EventDataType } from '../../NativeBridgeModule';

class MarketNoRtuStore {
    marketSymbols: ExtractMarketData[] = [];
    marketSymbolsGroupByCoinCode: { [key: string]: ExtractMarketData } = {};
    offlineSymbolsGroupByCoinCode: { [key: string]: ExtractMarketData } = {};
    rateTypeSymbol: string = 'USDT_USDC';
    isInnerTest: boolean = false;


    constructor() {
        makeObservable(this, {
            rateTypeSymbol: observable,
            isInnerTest: observable,
            fetchFullMarketSymbols: flow,
            fetchOfflineSymbols: flow,
            updateRateType: action,
            updateIsInnerTest: action,
            updateMarketSymbols: action,
            updateOfflineSymbols: action,
        })
    }

    * fetchFullMarketSymbols(routeObj: { key: string } | null = null) {
        HttpLogcater.httpLogcaterTest('fetchFullMarketSymbols--start', routeObj?.key)
        
        const marketSymbols: EventDataTypeWithLis<ExtractMarketData[]> = yield rtnClientInstance.network.getMarketDataDetail(routeObj?.key as string, null)

        const removeListener = marketSymbols.addRemoteListener?.((remoteResult: EventDataType<ExtractMarketData[]>) => {
            HttpLogcater.httpLogcaterTest('fetchFullMarketSymbols--remote-push:', Object.keys(remoteResult), remoteResult.data?.length)
            this.updateMarketSymbols(remoteResult)
            removeListener?.()
        })

        HttpLogcater.httpLogcaterDev('fetchFullMarketSymbols', marketSymbols.data)
        HttpLogcater.httpLogcaterTest('ticker接口币种信息获取条数,0表示失败', marketSymbols.data.length)
        this.updateMarketSymbols(marketSymbols)
    }

    updateMarketSymbols = (marketSymbols: EventDataType<ExtractMarketData[]>) => {
        const dirtyMarketSymbols = differenceBy(marketSymbols.data, this.marketSymbols, 'coinCode');
        HttpLogcater.httpLogcaterTest('updateMarketSymbols--difference', this.marketSymbols.length, marketSymbols.data.length,  dirtyMarketSymbols.map((item => {
            return item.coinCode
        })))
        if (marketSymbols.isSuccess && !isEmpty(marketSymbols.data)) {
            const sortData = marketSymbols.data.sort(function ({ sort: sortA }, { sort: sortB }) { return sortB - sortA })
            this.marketSymbols = sortData
            this.syncToRedux(sortData)
            MarketSymbolsCache.updateNoRtuMarketSymbols(sortData)
        }
    }

    *fetchOfflineSymbols(routeObj: { key: string } | null = null) {
        HttpLogcater.httpLogcaterDev('fetchOfflineSymbols--start', routeObj, "下线盘口信息")

        const offlineSymbols: EventDataTypeWithLis<ExtractMarketData[]> = yield rtnClientInstance.network.getOfflineMarketDataDetail(routeObj?.key as string)
        HttpLogcater.httpLogcaterDev('fetchOfflineSymbols--end', offlineSymbols.data.length, Object.keys(offlineSymbols))
        HttpLogcater.httpLogcaterTest('下线盘口信息', offlineSymbols.data.map((item => {
            return item.coinCode
        })))
        HttpLogcater.httpLogcaterTest('下线盘口信息-s', offlineSymbols.data?.length)

        const removeListener = offlineSymbols.addRemoteListener?.((remoteResult: EventDataType<ExtractMarketData[]>) => {
            HttpLogcater.httpLogcaterTest('下线盘口信息--push', remoteResult, remoteResult.data.map((item => {
                return item.coinCode
            })))
            this.updateOfflineSymbols(remoteResult)
            removeListener?.()
        })
        this.updateOfflineSymbols(offlineSymbols)
    }

    updateOfflineSymbols = (offlineSymbols: EventDataType<ExtractMarketData[]>) => {
        if (offlineSymbols.isSuccess && !isEmpty(offlineSymbols.data)) {
            const offlineSymbolsGroupByCoinCode: { [key: string]: ExtractMarketData } = {}
            offlineSymbols.data.forEach((item) => {
                offlineSymbolsGroupByCoinCode[item.coinCode] = item
            })
            this.offlineSymbolsGroupByCoinCode = offlineSymbolsGroupByCoinCode;
            JsSdkEventEmitInstance.emitEvent('jssdk_offline_symbols', offlineSymbolsGroupByCoinCode)
        }
    }

    updateRateType = (rateTypeSymbol: string) => {
        if(this.rateTypeSymbol !== rateTypeSymbol){
            HttpLogcater.httpLogcaterDev('updateRateType', rateTypeSymbol)
            HttpLogcater.httpLogcaterTest('更新基准汇率:' + rateTypeSymbol)
            this.rateTypeSymbol = rateTypeSymbol;
        }
    }

    updateIsInnerTest = (isInnerTest: boolean) => {
        this.isInnerTest = isInnerTest;
    }

    // 同步到redux
    private syncToRedux = (sortData: ExtractMarketData[]) => {
        const marketSymbolsGroupByCoinCode: { [key: string]: ExtractMarketData } = {}
        sortData.forEach((item) => {
            marketSymbolsGroupByCoinCode[item.coinCode] = item;
            if (item.coinCode === 'BTC_USDT') {
                HttpLogcater.httpLogcaterTest("非实时价格比特币价格更新了:" + "精度symbolPrecision:" + item.symbolPrecision + ", 价格:" + item.priceLast, item.allowTrade)
            }
            if (item.coinCode === 'MATIC_USDT') {
                HttpLogcater.httpLogcaterTest("非实时价格MATIC_USDT更新了:" + "精度symbolPrecision:" + item.symbolPrecision + ", 价格:" + item.priceLast, item.allowTrade)
            }
        })
        const refreshState: any = { marketSymbolsGroupByCoinCode, marketSymbols: sortData }
        
        this.marketSymbolsGroupByCoinCode = refreshState.marketSymbolsGroupByCoinCode;
        this.marketSymbols = refreshState.marketSymbols;
        refreshState.rateType = this.rateTypeSymbol;
        JsSdkEventEmitInstance.emitEvent('jssdk_ticker_marketsymbols', refreshState)
    }

}

export default new MarketNoRtuStore()
