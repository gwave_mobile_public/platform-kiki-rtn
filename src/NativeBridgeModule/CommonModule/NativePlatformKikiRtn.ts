import type { TurboModule } from 'react-native';
import { TurboModuleRegistry } from 'react-native';

export interface Spec extends TurboModule {
  getOrCreatePageScope(): Promise<string>;
  destroyPageScope(pageScopeId: string): Promise<boolean>;
  initApplicationEnv(envType: String, params: { jwtToken?: string, innerTest: boolean, deviceId: string }): void;
  updateJwtToken(jwtToken: String | null): void;
  updateInnerTest(isInnerTest: boolean): void;
}

export default TurboModuleRegistry.getEnforcing<Spec>('PlatformKikiRnCommonModule');
