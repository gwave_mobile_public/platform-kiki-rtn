
import { NativeModules } from 'react-native';

import { createSafeModule, isTurboModuleEnabled } from '../utils';
import type { Spec } from './NativePlatformKikiRtn'
// console.log('1111', isTurboModuleEnabled)

const PlatformKikiRtnModule = isTurboModuleEnabled
    ? require('./NativePlatformKikiRtn').default
    : NativeModules.PlatformKikiRnCommonModule;

    
const PlatformKikiRtn = createSafeModule<Spec>(PlatformKikiRtnModule)


export { PlatformKikiRtn as PlatformKikiCommonRtn }
