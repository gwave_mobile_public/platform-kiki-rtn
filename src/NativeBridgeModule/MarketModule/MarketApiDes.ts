

/**
 * ExtractMarketData
 * @typedef {object} ExtractMarketData
 * @property {number} sort.required
 * @property {string} coinCode.required
 * @property {string} priceLast.required
 * @property {string} volume.required
 * @property {string} quoteVolume.required
 * @property {string} riseAndFall.required
 * @property {string} highPrice.required
 * @property {string} lowPrice.required
 * @property {number} precision.required
 * @property {number} volumePrecision.required
 * @property {number} marketPricePrecision.required
 * @property {number} tradePrecision.required
 * @property {number} tradeInputPrecision.required
 * @property {number} tradeVolumePrecision.required
 * @property {number} symbolPrecision.required
 * @property {number} orderBaseMax.required
 * @property {number} marketFreezeBuffer.required
 * @property {number} orderBaseMin.required
 * @property {number} orderQuoteMax.required
 * @property {number} orderQuoteMin.required
 * @property {number} ask.required
 * @property {number} bid.required
 * @property {number} orderMax.required
 * @property {number} orderMin.required
 * @property {string} symbol.required
 * @property {number[]} priceList.required
 * @property {number} priceLastRise.required
 * @property {string} baseCurrencyKey.required
 * @property {boolean} allowTrade.required
 * @property {number} state.required
 */

/**
 * @typedef {object} RtnMarketSymbols
 * @property {ExtractMarketData[]} data.required
 * @property {boolean | null} isSuccess.required 
 * @property {string} eventUpdateEventId
 */

/**
 * @typedef {object} ExtractZoneBean
 * @property {string} key.required
 * @property {string} name.required
 * @property {string[]} picPaths.required
 * @property {object | null} currencies
 * @property {number} order.required
 * @property {number} status.required
 * @property {string | null} leadingCurrency
 * @property {number} fallCount.required
 * @property {number} leadingPercent.required
 * @property {number} riseCount.required
 * @property {number} percent.required
 * @property {string[] | null} bars
 */

/**
 * @typedef {object} RtnExtractZoneBean
 * @property {ExtractZoneBean[]} data.required
 * @property {boolean | null} isSuccess.required 
 * @property {string} eventUpdateEventId
 */


/**
 * @typedef {object} ZoneHeader
 * @property {number} allRiseAndFall.required
 * @property {number} riseCount.required
 * @property {number} fallCount.required
 * @property {string} leadingCurrency.required
 * @property {number} leadingCurrencyRise.required
 */

/**
 * @typedef {object} ExtractZoneDetail
 * @property {ZoneHeader} header.required
 * @property {ExtractMarketData[]} symbolList.required
 */

/**
 * @typedef {object} RtnExtractZoneDetail
 * @property {ExtractZoneDetail} data.required
 * @property {boolean | null} isSuccess.required
 * @property {string} eventUpdateEventId
 */