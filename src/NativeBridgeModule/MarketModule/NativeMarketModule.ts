import type { TurboModule } from 'react-native';
import { TurboModuleRegistry } from 'react-native';
import type { EventDataType, ExtractMarketData, ExtractZoneBean, ExtractZoneDetail } from '../NativeTypes';

export interface Spec extends TurboModule {

  /*  *******************注册相关代码*************************  */
  // 全局的scopeId 在registerMarketWebSocket 后面进行调用，整个app只调用一次
  preloadMarketLocalDB(): Promise<Boolean>;

  /* ********************获取本地缓存************************  */

  /*  ********************获取订阅eventName************************  */

  connectMarketDataDetails(scopeId: String, symbolNameArray: string[] | null): Promise<string>;
  connectOrderBook(scopeId: String): Promise<string>;
  connectDepth(scopeId: String): Promise<string>;
  connectZoneDetail(scopeId: String, zoneKey: string, sortName: string, sortType: string): Promise<string>;
  connectOrder(scopeId: String): Promise<String>

  /* **************** Api接口接口 *******************  */

  /**
   * GET market/PlatformKikiMarketRtn/getMarketDataDetail
   * @tags network
   * @operationId getMarketDataDetail
   * @param {string} routeKey.query.required - routeKey
   * @param {string[] | null} symbolNames.query.required - symbolNames null 表示获取全部
   * @return {RtnMarketSymbols} 200 - success response - application/json
   */
  getMarketDataDetail(scopeId: string, symbolNames: string[] | null | undefined): Promise<EventDataType<ExtractMarketData[]>>;

  /**
   * GET market/PlatformKikiMarketRtn/getOfflineMarketDataDetail
   * @tags network
   * @operationId getOfflineMarketDataDetail
   * @param {string} routeKey.query.required - routeKey
   * @return {RtnMarketSymbols} 200 - success response - application/json
   */
  getOfflineMarketDataDetail(scopeId: String): Promise<EventDataType<ExtractMarketData[]>>;

  /**
   * GET market/PlatformKikiMarketRtn/getZoneListDetails
   * @tags network
   * @operationId getZoneListDetails
   * @param {string} routeKey.query.required - routeKey
   * @param {number} maxRefreshTimeout.query.required - maxRefreshTimeout 
   * @return {RtnExtractZoneBean} 200 - success response - application/json
   */
  getZoneListDetails(scopeId: string, maxRefreshTimeout: number): Promise<EventDataType<ExtractZoneBean[]>>;
  /**
   * GET market/PlatformKikiMarketRtn/getZoneDetail
   * @tags network
   * @operationId getZoneDetail
   * @param {string} routeKey.query.required - routeKey
   * @param {string} zoneKey.query.required - zoneKey
   * @param {string} sortName.query.required - sortName
   * @param {string} sortType.query.required - sortType
   * @return {RtnExtractZoneDetail} 200 - success response - application/json
   */
  getZoneDetail(scopeId: string, zoneKey: string, sortName: string, sortType: string): Promise<EventDataType<ExtractZoneDetail>>
  /**
   * GET market/PlatformKikiMarketRtn/getZoneDetailSymbols
   * @tags network
   * @operationId getZoneDetailSymbols
   * @param {string} routeKey.query.required - routeKey
   * @param {string} zoneKey.query.required - zoneKey
   * @param {string} sortName.query.required - sortName
   * @param {string} sortType.query.required - sortType
   * @return {RtnMarketSymbols} 200 - success response - application/json
   */
  getZoneDetailSymbols(scopeId: string, zoneKey: string, sortName: string, sortType: string): Promise<EventDataType<Array<ExtractMarketData>>>
  /**
   * GET market/PlatformKikiMarketRtn/getMarketDetailTabZoneDataList
   * @tags network
   * @operationId getMarketDetailTabZoneDataList
   * @param {string} routeKey.query.required - routeKey
   * @param {number} num.query.required - num
   * @return {RtnExtractZoneBean} 200 - success response - application/json
   */
  getMarketDetailTabZoneDataList(scopeId: string, num: number): Promise<EventDataType<ExtractZoneBean[]>>;

  /**
   * GET market/PlatformKikiMarketRtn/getMarketDetailByCondition
   * @tags network
   * @operationId getMarketDetailByCondition
   * @param {string} routeKey.query.required - routeKey
   * @param {string} baseCurrency.query.required - baseCurrency
   * @param {string} zoneKey.query.required - zoneKey
   * @param {string} sortName.query.required - sortName
   * @param {string} sortType.query.required - sortType
   * @return {RtnMarketSymbols} 200 - success response - application/json
   */
  getMarketDetailByCondition(scopeId: String,
    baseCurrency: String,
    zoneKey: String,
    sortName: String,
    sortType: String,
  ): Promise<EventDataType<ExtractMarketData[]>>;
  /* ******************* END *************************  */

  sendMsgToWebsocket(scopeId: String, msg: String): void;




  /* ******************* END *************************  */

}

export default TurboModuleRegistry.getEnforcing<Spec>('PlatformKikiRnMarketModule');
