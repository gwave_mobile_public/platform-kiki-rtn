
import { NativeModules } from 'react-native';

import { createSafeModule, isTurboModuleEnabled } from '../utils';
import type { Spec } from './NativeMarketModule'
// console.log('isTurboModuleEnabled', isTurboModuleEnabled)
const MarketModule = isTurboModuleEnabled
    ? require('./NativeMarketModule').default
    : NativeModules.PlatformKikiRnMarketModule;



// console.log('MarketModule-market:', MarketModule)
const SafeMarketModule = createSafeModule<Spec>(MarketModule)


export { SafeMarketModule as PlatformKikiMarketRtn }
