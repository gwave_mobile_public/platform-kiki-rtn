
export interface ExtractMarketData { //
    sort: number, //symbol
    coinCode: string,
    priceLast: string, //
    volume: string, //
    quoteVolume: string, //
    riseAndFall: string, //
    highPrice: string, //
    lowPrice: string, //
    precision: number,
    volumePrecision: number, // symbol
    marketPricePrecision: number, // symbol
    tradePrecision: number, // symbol
    tradeInputPrecision: number, // symbol
    tradeVolumePrecision: number, // symbol
    symbolPrecision: number, // symbol
    orderBaseMax: number,
    marketFreezeBuffer: number, // symbol
    orderBaseMin: number,
    orderQuoteMax: number,
    orderQuoteMin: number,
    ask: number,
    bid: number,
    orderMax: number, // symbol
    orderMin: number, // symbol
    symbol: string, // symbol
    priceList: number[],
    priceLastRise: number,
    baseCurrencyKey: string,
    allowTrade: boolean,
    state:number
}
export interface ExtractZoneBean{
  key:string,
  name: string,
  picPaths:Array<string>,
  currencies:ZoneCoinKeyMap | null | undefined,
  order:number,
  status:number,
  leadingCurrency:string | null | undefined,
  fallCount:number,
  leadingPercent:number,
  riseCount:number,
  percent:number,
  bars:Array<string> | null | undefined

}

export interface ExtractZoneDetail{
  header:ZoneHeader,
  symbolList:Array<ExtractMarketData>
}

type ZoneHeader ={
  allRiseAndFall:number,
  riseCount:number,
  fallCount:number,
  leadingCurrency:string,
  leadingCurrencyRise:number
}

type ZoneCoinKeyMap={
  [key:string]:string
}
