
import type { ExtractMarketData, ExtractZoneBean, ExtractZoneDetail } from './MarketType'

export { ExtractMarketData }
export { ExtractZoneBean }
export { ExtractZoneDetail }
export type ScopeIdType = string


export type EventDataType<T> = {
    data: T,
    isSuccess?: boolean,
    eventUpdateEventId?: string,
}

export type EventDataTypeWithLis<T> = {
    data: T,
    isSuccess?: boolean
    eventUpdateEventId?: string,
    addRemoteListener?: (listener: (data: EventDataType<T>) => void) => ()=>void,
}
