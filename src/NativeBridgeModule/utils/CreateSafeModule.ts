import { Platform } from 'react-native';



const LINKING_ERROR =
  `The package 'platform-kiki-rtn' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

export function createSafeModule<T>(moduleObject: T): T {


  const result = moduleObject
    ? moduleObject
    : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );
  return result as T
}