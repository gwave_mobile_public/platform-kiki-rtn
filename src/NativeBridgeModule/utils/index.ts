import { createSafeModule } from './CreateSafeModule';


// @ts-expect-error
const isTurboModuleEnabled = global.__turboModuleProxy != null;

export { createSafeModule, isTurboModuleEnabled }
