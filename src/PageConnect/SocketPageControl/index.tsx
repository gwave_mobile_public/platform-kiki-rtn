import React, { useMemo, useRef } from 'react'
import { usePageStateRn } from '@kiki/f2e.khooks.rn'
import { useConnectSocket } from '../../MarketSymbolsLegacy'
import type { SocketModuleTypes } from '../../SocketModule/PushEventName'
import JsSdkEventEmit from '../../utils/JsSdkEventEmit'
import {
    useContextOnChild,
    useParentProvider
} from '@kiki/f2e.khooks.context'
import AbortSignal from '../../utils/AbortSignal'
import { useNavigation, useRoute } from '@react-navigation/native'

type ConnectType = (abortSignal: AbortSignal) => Promise<void>
type DistoryConnectType = () => void

export type ConnectApiConfig = {
    aliasName: SocketModuleTypes,
    args: any[] | ((params: object) => any[]),
}

function createConnectCollect(apiConfigs: ConnectApiConfig[], pageEventEmitter: JsSdkEventEmit) {
    const createConnects: ConnectType[] = []
    const destoryConnects: DistoryConnectType[] = []
    apiConfigs.forEach((apiConfig: ConnectApiConfig) => {
        // @ts-expect-error
        const [createConnect, destoryConnect] = useConnectSocket(apiConfig.aliasName, pageEventEmitter, ...apiConfig.args)
        // @ts-expect-error
        createConnects.push(createConnect!)
        // @ts-expect-error
        destoryConnects.push(destoryConnect!)
    })

    const createConnectFunc = () => {
        const abortSignal = new AbortSignal()
        createConnects.forEach((createConnect) => {
            createConnect(abortSignal)
        })
        console.log("SocketPageControl-createConnectFunc", apiConfigs, destoryConnects.length)
        return abortSignal
    }

    const destoryConnectFunc = () => {
        destoryConnects.forEach((destoryConnect) => {
            destoryConnect()
        })
        console.log("SocketPageControl-destoryConnectFunc", apiConfigs, destoryConnects.length)
    }
    return [createConnectFunc, destoryConnectFunc]
}

/**
 * 不导出就是为了防止乱用
 * @param socketTypes
 */
function useSocketPageControl(apiConfigs: ConnectApiConfig[], pageEventEmitter: JsSdkEventEmit) {
    // const [createConnect, destoryConnect] = useConnectSocket('socket.marketsymbol', null)
    const [createConnect, destoryConnect] = createConnectCollect(apiConfigs, pageEventEmitter)
    const abortSignalRef = useRef<AbortSignal>()

    const currentFocusRef = useRef(false)
    const routeKey = useRoute().key
  

    const releaseConnect = ()=>{
        abortSignalRef.current?.abort()
        abortSignalRef.current = undefined
        destoryConnect?.()
        // 注销native 定时器
    }

    const onResumePage = (isFromPage: boolean) => {
        // 切换到后台逻辑 底层进行了实现
        if (isFromPage) {
            console.log("kskkspage-onResumePage", routeKey)

          currentFocusRef.current = true
        // @ts-expect-error
          abortSignalRef.current = createConnect?.()
        }
    }


    const onPausePage = (isFromPage: boolean) => {
        // 切换到后台逻辑 底层进行了实现
        if (isFromPage) {
            console.log("kskkspage-onPausePage", routeKey, currentFocusRef.current)
            if(!currentFocusRef.current){
                return 
            }
            console.log("kskkspage-onPausePage", routeKey, 'msksksks')

            currentFocusRef.current = false
            releaseConnect()
        }
    }

    // 写在这为了保证时序问题
    const navigation = useNavigation();
    const valueToReturn = navigation.isFocused();

    // 弥补blur状态丢失的问题
    if (!valueToReturn && currentFocusRef.current !== valueToReturn) {
        currentFocusRef.current = false
        releaseConnect()
        console.log("kskkspage-valueToReturn", routeKey)
    }
    usePageStateRn(onResumePage, onPausePage)

}

/**
 * 控制socket按需加载
 * @param socketTypes
 * @param needInitFullMarketSymbol
 * @returns
 */
export function SocketAutoConnect(apiConfigs: ConnectApiConfig[]) {

    return (WrappedComponent: any) => {

        function RenderFunc(childProps: object) {
            // const pushLisCallBack = useMemoizedFnCommon((aliasName: SocketModuleTypes, eventData: any) => {
            // })
            const PageEmitterProvider: Element = useParentProvider('jssdk_evetEmit')


            const pageEventEmitter = useMemo(() => {
                return new JsSdkEventEmit()
            }, [])

            const finalConfigs = apiConfigs.map((config :ConnectApiConfig) => {
              if (typeof config.args === 'function') {
                const args = config.args(childProps) || []
                return {
                  ...config,
                  args
                }
              }
              return config
            })
            useSocketPageControl(finalConfigs, pageEventEmitter)
            const renderedWrappedComponent = useMemo(
                () => (
                    <WrappedComponent
                        {...childProps}
                        addJsSdkSocketListener={pageEventEmitter.addEventListener}
                    />
                ),
                [WrappedComponent, childProps]
            )
            // Provider不能被  useMemo 包裹; 否则会导致 WrappedComponent创建多次
            return (<PageEmitterProvider
                depsValues={{ pageEventEmitter }}
            >
                {renderedWrappedComponent}
            </PageEmitterProvider>)
        }

        return React.memo(RenderFunc)
    }
}

export function useJsSdkSocketListener() {
    const { pageEventEmitter }: { pageEventEmitter: JsSdkEventEmit } = useContextOnChild('jssdk_evetEmit')
    return pageEventEmitter?.addEventListener
}
