
import type { NavigationState } from "@react-navigation/native";

export function parseNavigationRouteKeys(routeDatas: NavigationState | undefined): string[] {
    const routeKeys: string[] = [];
    const parseItem = (routeItem: any) => {
        if (!!routeItem && routeItem.key) {
            routeKeys.push(routeItem.key);
            if (!!routeItem.routes) {// routeItem2
                routeItem.routes.map((childItem: any) => {
                    parseItem(childItem)
                })
            }
            if (!!routeItem.state) {// routeItem2
                parseItem(routeItem.state)
            }
        }
    }
    parseItem(routeDatas)
    return routeKeys
}