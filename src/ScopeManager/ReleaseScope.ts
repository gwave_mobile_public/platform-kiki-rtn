import {NormalScopeIdManager, SockcetScopeIdManager} from "./ScopeIdManager";
import type { NavigationState } from "@react-navigation/native";
import { parseNavigationRouteKeys } from "./NavigationRouteKeys";


export function autoReleaseScopeOnNavigationChange(routeDatas: NavigationState | undefined) {
    const routeKeys = parseNavigationRouteKeys(routeDatas);
    NormalScopeIdManager.tryReleaseDirtyScopes(routeKeys)
    SockcetScopeIdManager.tryReleaseDirtyScopes(routeKeys)
}

export function releaseGlobalScope() {// 可能不需要， 目前RN是单容器，容器死，底层自动就释放了； 保险起见做个释放； 
    NormalScopeIdManager.deleteGlobalScopeId()
    SockcetScopeIdManager.deleteGlobalScopeId()
}