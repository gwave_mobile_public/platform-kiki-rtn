import difference from "lodash/difference";
import type { ScopeIdType } from "../NativeBridgeModule/NativeTypes";
import { PlatformKikiCommonRtn } from "../NativeBridgeModule";
import type AbortSignal from '../utils/AbortSignal'

type ScopeIdMapType = {
    [key: string]: ScopeIdType[],
}

class ScopeIdManager {
    protected globalScopeId: ScopeIdType| null;
    protected scopeIdMap: ScopeIdMapType;
    protected typeName: string;

    constructor(typeName: string) {
        this.typeName = typeName;
        this.globalScopeId = null
        this.scopeIdMap = {}
        this._initGlobalScopeId()
    }


    async getScopeIdByRouteKey(routeKey?: string, abortSignal?:AbortSignal) {
        if (!!routeKey) {
            // 底层会直接释放scopeId,所以这里scopeId 跟routeKey 是多对一的关系
            return await this._createScopeIdByRouteKey(routeKey, abortSignal)
        }
        return await this._getGlobalScopeId()
    }

    tryReleaseDirtyScopes(currentApprouteKeys: string[]) {
        const scopeRouteKeys = Object.keys(this.scopeIdMap)
        const dirtyRouteKeys = difference(scopeRouteKeys, currentApprouteKeys);
        dirtyRouteKeys.map((dirtyRouteKey:string) => {
            this._deleteScopeId(dirtyRouteKey)
        })
    }

    deleteGlobalScopeId() {
        if(!!this.globalScopeId){
            PlatformKikiCommonRtn.destroyPageScope(this.globalScopeId)
            this.globalScopeId = null
        }
    }

     _deleteScopeId(routeKey?: string) {
        if (!!routeKey) {
            if (!!this.scopeIdMap[routeKey]) {
                this.scopeIdMap[routeKey]!.map(destroyId => {
                    PlatformKikiCommonRtn.destroyPageScope(destroyId)
                    console.log("SocketPageControl-ScopeIdManager-deleteScopeId", routeKey, destroyId)
                })

                delete this.scopeIdMap[routeKey]
            }
        }
    }

    // 打socket abort打断时候补丁
    deleteSingleScopeId(destroyId: string) {
        PlatformKikiCommonRtn.destroyPageScope(destroyId)
    }

    async _initGlobalScopeId() {
        this.globalScopeId = await this._createGlobalScopeId()
    }

    private async _getGlobalScopeId() {
        if (!!this.globalScopeId) {
            return this.globalScopeId
        }
        this.globalScopeId = await this._createGlobalScopeId()
        return this.globalScopeId
    }
    private async _createGlobalScopeId() {
        return await PlatformKikiCommonRtn.getOrCreatePageScope()
    }

    private async _createScopeIdByRouteKey(routeKey: string, abortSignal?:AbortSignal) {
        const newScopeId = await PlatformKikiCommonRtn.getOrCreatePageScope()
        // 点击太快进行销毁
        if(abortSignal?.isAborted){
            PlatformKikiCommonRtn.destroyPageScope(newScopeId)
            return null
        }
        this.addScopeId(routeKey, newScopeId)
        return newScopeId
    }

    private addScopeId(routeKey: string, scopeId: ScopeIdType) {
        if(!this.scopeIdMap[routeKey]){
            this.scopeIdMap[routeKey] = []
        }
        this.scopeIdMap[routeKey]?.push(scopeId)
    }
}

const NormalScopeIdManager = new ScopeIdManager('nettype')
const SockcetScopeIdManager = new ScopeIdManager('sockettype')

export {NormalScopeIdManager, SockcetScopeIdManager}