import { autoReleaseScopeOnNavigationChange, releaseGlobalScope } from "./ReleaseScope";
import {NormalScopeIdManager, SockcetScopeIdManager} from "./ScopeIdManager";
import { useSafeRoute } from './useSafeRoute'

export type RouteType = {key: string}


export { autoReleaseScopeOnNavigationChange, releaseGlobalScope, NormalScopeIdManager, SockcetScopeIdManager, useSafeRoute }

