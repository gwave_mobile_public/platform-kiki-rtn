import { useMemoizedFnCommon,  useUnmountCommon } from '@kiki/f2e.khooks.common';


import { releaseGlobalScope } from '../../ScopeManager';

import { MarketSymbolsCache } from '../../SocketModule'

import type { ExtractMarketData } from '../../NativeBridgeModule/NativeTypes/MarketType';
import { SocketLogcater } from '../../JsSdkkLogCat';
import { useEffect } from 'react';
import { JsSdkEventEmitInstance } from '../../utils/JsSdkEventEmit';
import {SocketEventManager} from '../../SocketModule';
import { RemoteLisManager } from '../../HttpRequest';

let marketItemMediumUpdateTs = new Date().getTime()

const MarketEventMediumUpdateInterval = 10000

function JsSdkComponent({ dispatch }: { dispatch: (data: any) => void }) {
    // 理论上不需要realse, 为以后多模块做准备
    const rtuCallBack = useMemoizedFnCommon((marketSymbols: ExtractMarketData[]) => {
        
        const filterMarketSymbols = MarketSymbolsCache.updateRtuMarketSymbols(marketSymbols)

        SocketLogcater.socketLogcatManagerDev('rtuCallBack', marketSymbols.length, filterMarketSymbols.length)
        SocketLogcater.socketLogcatManagerPush('native推送给jssdk数据条数', marketSymbols.length)
        SocketLogcater.socketLogcatManagerPush('jssdk推送给界面数据条数', filterMarketSymbols.length)

        dispatch?.({
            type: 'FULL_MARKET_SYMBOLS_REFRESH',
            marketSymbols: filterMarketSymbols
        })


        if (new Date().getTime() - marketItemMediumUpdateTs > MarketEventMediumUpdateInterval) {

            dispatch?.({
                type: 'FULL_MARKET_SYMBOLS_MEDIUM_REFRESH',
                marketSymbols: filterMarketSymbols
            })
            marketItemMediumUpdateTs = new Date().getTime()
        }
    })

    useEffect(()=>{
        const remoteParentLis = RemoteLisManager.addParentEventLis()
        const socketParentLis = SocketEventManager.addParentEventLis()
        const marketSymbolsLis = JsSdkEventEmitInstance.addEventListener('jssdk_marketsysbols_push', rtuCallBack)
        return ()=>{
            remoteParentLis?.remove?.();
            socketParentLis?.remove?.();
            marketSymbolsLis?.();
        }
    }, [])

    useUnmountCommon(() => {
        releaseGlobalScope()
        JsSdkEventEmitInstance.removeAllListener();
        RemoteLisManager.removeAllRemoteHttpLis()
        SocketEventManager.removeAllRemoteHttpLis()
    })

    return null
}


export { JsSdkComponent }

