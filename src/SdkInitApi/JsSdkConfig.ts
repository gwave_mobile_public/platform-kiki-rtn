import { PlatformKikiCommonRtn } from "../NativeBridgeModule";
import { HttpLogcater } from '../JsSdkkLogCat';
import { PlatformKikiMarketRtn } from '../NativeBridgeModule';

class JsSdkConfig {
    protected jwtToken: string;
    protected envType: string;
    protected deviceId: string;
    protected roomType: string;
    constructor() {
        this.jwtToken = '';
        this.envType = '';
        this.deviceId = '';
        this.roomType = 'marketData';
    }

    initJsSdk(jwtToken: string = '', envType: string = '', deviceId: string) {
        this.envType = envType;
        this.jwtToken = jwtToken;
        this.deviceId = deviceId;
        if (!!this.envType) {
            PlatformKikiCommonRtn.initApplicationEnv(this.envType, { jwtToken, innerTest: false, deviceId: this.deviceId })
        } else {
            throw new Error('JsSdkConfig的envType不能为空')
        }
    }

    
    async initLocalDB(): Promise<Boolean> {
        const result = await PlatformKikiMarketRtn.preloadMarketLocalDB()
        return result
    }

    switchJwtToken(jwtToken: string = '') {
        this.jwtToken = jwtToken;
        HttpLogcater.httpLogcaterDev('switchJwtToken-native', this.jwtToken)
        HttpLogcater.httpLogcaterTest('底层切换用户token' + this.jwtToken)
        PlatformKikiCommonRtn.updateJwtToken(this.jwtToken)
    }

    updateInnerTest(isInnerTest = false) {
        HttpLogcater.httpLogcaterDev('updateInnerTest-native', isInnerTest)
        HttpLogcater.httpLogcaterTest('底层更新内侧用户:状态' + isInnerTest)
        PlatformKikiCommonRtn.updateInnerTest(isInnerTest)
    }

    getJwtToken() {
        return this.jwtToken;
    }

    getDeviceId() {
        return this.deviceId;
    }

    getRoomType() {
        return this.roomType;
    }

    isProdProject() {
        return this.envType === 'prod' || this.envType === 'pro'
            || this.envType === 'prodgreen'
    }
}

export default new JsSdkConfig()


