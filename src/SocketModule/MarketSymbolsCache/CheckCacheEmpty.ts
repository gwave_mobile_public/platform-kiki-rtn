

import { HttpLogcater } from '../../JsSdkkLogCat';
import {JsSdkEventEmitInstance} from '../../utils/JsSdkEventEmit'

const fetchTimeSpace = 10000; // 每次尝试请求间隔的时间 10秒
/**
 * 当有推送时候，而内存缓存为空时候进行一次请求
 */
class CheckCacheEmpty {

    /**
     * 推送数据推了两次; 初始化必推了一次，这个是个bug
     * 
     */
    protected pushTime: number;

    // socket
    protected lastTryTime: number;

    // 请求次数
    protected fetchTimes: number;

    constructor() {
        this.pushTime = 0
        this.lastTryTime = new Date().getTime()
        this.fetchTimes = 0
    }

    checkRetryInitCache(needReinit = false) {
        HttpLogcater.httpLogcaterTest('糟糕数据ticker和symbol数据为空')
        if (needReinit) {
            // 间隔大于10秒
            if ((new Date().getTime() - this.lastTryTime) > fetchTimeSpace) {
                this.pushTime = Math.min(this.pushTime + 1, 10)
                // 推送数据推了两次; 初始化必推了一次，这个是个bug
                if (this.pushTime > 1 && this.fetchTimes < 5) {
                    this.lastTryTime = new Date().getTime()
                    this.fetchTimes = this.fetchTimes + 1
                    HttpLogcater.httpLogcaterDev('checkRetryInitCache', { fetchTimes: this.fetchTimes, pushTime: this.pushTime })
                    HttpLogcater.httpLogcaterTest('非常糟糕!!socket有推送,但缓存为空，尝试初始化缓存', { fetchTimes: this.fetchTimes, pushTime: this.pushTime })
                    JsSdkEventEmitInstance.emitEvent("jssdk_tryreinit_cache", this.fetchTimes)
                }
            }
        }
    }

}

export default new CheckCacheEmpty()