import type { ExtractMarketData } from '../../NativeBridgeModule/NativeTypes'
import { findIndex, isEmpty, once } from 'lodash';
import CheckCacheEmpty from './CheckCacheEmpty'
import {JsSdkEventEmitInstance} from '../../utils/JsSdkEventEmit'
 
import { HttpLogcater, SocketLogcater } from '../../JsSdkkLogCat';
/**
 * 作为  RN 升级到0.69前 做内存缓存, 主要解决同步使用问题;  升级完用getCacheMethod 同步方法替代
 */


class MarketSymbolsCache {
    protected marketSymbols: ExtractMarketData[];
    protected safeCoinCodes: string[];
    protected unSafeCoinCodes: string[];
    protected _notifyDbInitSuccessOnce;

    // hasSetCache为了解决remote 比cache快
    protected hasSetCache: boolean;
    constructor() {
        this.marketSymbols = []
        this.hasSetCache = false
        this.safeCoinCodes = []
        this.unSafeCoinCodes = []
        this._notifyDbInitSuccessOnce = once(this._notifyDbInitSuccess)
    }

    updateRtuMarketSymbols(marketSymbols: ExtractMarketData[]): ExtractMarketData[] {
        if (isEmpty(this.marketSymbols)) {
            CheckCacheEmpty.checkRetryInitCache(true)
            return [];
        }
        // 解决推送比 tiker数据多或少问题问题
        let newMarketSymbols = this.marketSymbols
        marketSymbols.forEach((item) => {
            let orgIndex = findIndex(this.marketSymbols, { coinCode: item.coinCode })
            if (item.coinCode === 'BTC_USDT') {
                SocketLogcater.socketLogcatManagerPush("BTC_USDT价格更新了:" + item.priceLast, item.allowTrade)
            }
            if (orgIndex > -1) {
                newMarketSymbols[orgIndex] = item
                let firstPush = this.safeCoinCodes.indexOf(item.coinCode) === -1
                if (firstPush) {
                    this.safeCoinCodes.push(item.coinCode)
                }
            } else {
                let firstPush = this.unSafeCoinCodes.indexOf(item.coinCode) === -1
                if (firstPush) {
                    this.unSafeCoinCodes.push(item.coinCode)
                }
            }

        })
        console.log('updateRtuMarketSymbols:-push', this.safeCoinCodes.length)
        console.log('updateRtuMarketSymbols:-push--remove', this.unSafeCoinCodes.length)
        this._updateMarketSymbols(newMarketSymbols);
        return newMarketSymbols

    }



    updateNoRtuMarketSymbols(marketSymbols: ExtractMarketData[]) {
        // 业务肯定不能为空
        // sortBy(marketSymbols, 'sort')
        this._updateMarketSymbolsSort(marketSymbols)
    }


    updateCacheMarketSymbols(marketSymbols: ExtractMarketData[]) {
        if (!this.hasSetCache) {
            this._updateMarketSymbolsSort(marketSymbols)
        }
    }


    /**
     * 
     */
    _notifyDbInitSuccess() {
        HttpLogcater.httpLogcaterTest('useConnectSocket', '通知数据初始化完成:_notifyDbInitSuccess')
        JsSdkEventEmitInstance.emitEvent('jssdk_initcache_success', null)
    }

    /**
     * 目前底层是倒序的，所以非实时是要排序的，实时数据是根据非实时数据为基准
     * @param marketSymbols 
     */
    _updateMarketSymbolsSort(marketSymbols: ExtractMarketData[]) {
        // this._updateMarketSymbols(marketSymbols.sort(function ({ sort: sortA }, { sort: sortB }) { return sortB - sortA }))
        this._updateMarketSymbols(marketSymbols)
    }

    _updateMarketSymbols(marketSymbols: ExtractMarketData[]) {
        if (!isEmpty(marketSymbols)) {
            this.hasSetCache = true
            this.marketSymbols = marketSymbols
            this._notifyDbInitSuccessOnce()
        }
    }

    getMarketSymbols() {
        return this.marketSymbols || []
    }

    getHasInit() {
        HttpLogcater.httpLogcaterTest('检测数据是否初始化完成:' + this.hasSetCache)
        return this.hasSetCache
    }

}


export default new MarketSymbolsCache()