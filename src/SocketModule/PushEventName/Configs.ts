
import { PlatformKikiMarketRtn } from "../../NativeBridgeModule";

const ConfigSocketApis = {
    'socket.marketsymbol': {
        queryUrlMethod: PlatformKikiMarketRtn.connectMarketDataDetails,
    },
    'socket.orderBook': {
      queryUrlMethod: PlatformKikiMarketRtn.connectOrderBook,
    },
    'socket.orderBook.depth': {
      queryUrlMethod: PlatformKikiMarketRtn.connectDepth,
    },
    'socket.zone.detail': {
      queryUrlMethod: PlatformKikiMarketRtn.connectZoneDetail,
    },
    'socket.order': {
      queryUrlMethod: PlatformKikiMarketRtn.connectOrder,
    },
}

const ConfigSocketOperateApis = {
  'socket.send.message': {
    operateMethod: PlatformKikiMarketRtn.sendMsgToWebsocket
  }
}

type SocketModuleTypes = keyof typeof ConfigSocketApis
type SocketOperateModuleTypes = keyof typeof ConfigSocketOperateApis

// symbolNameArray: string[] | null

// zoneKey:string,sortName:string,sortType:string

export { ConfigSocketApis, ConfigSocketOperateApis }
export { SocketModuleTypes, SocketOperateModuleTypes }


