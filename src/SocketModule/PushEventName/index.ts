import { ConfigSocketApis, ConfigSocketOperateApis } from "./Configs";
import type { SocketModuleTypes, SocketOperateModuleTypes } from './Configs'

export function getConnectFunc(aliasName: SocketModuleTypes) {
    return ConfigSocketApis[aliasName].queryUrlMethod
}


export function getOperateFunc(aliasName: SocketOperateModuleTypes) {
    return ConfigSocketOperateApis[aliasName].operateMethod
}


export { SocketModuleTypes, SocketOperateModuleTypes }
