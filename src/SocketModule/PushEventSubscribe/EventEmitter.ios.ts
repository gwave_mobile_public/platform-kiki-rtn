

import {
    NativeModules,
    NativeEventEmitter,  //导入NativeEventEmitter模块
} from 'react-native';

var PlatformKikiRnMarketModule = NativeModules.PlatformKikiRnMarketModule;
const MarketDeviceEventEmitterIos = new NativeEventEmitter(PlatformKikiRnMarketModule);  //创建自定义事件接口


export { MarketDeviceEventEmitterIos as MarketDeviceEventEmitter }


