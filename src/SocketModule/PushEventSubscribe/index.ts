import { SockcetScopeIdManager, useSafeRoute } from '../../ScopeManager';
import { getConnectFunc } from '../PushEventName'
import { useCallback, useRef } from 'react';
import type { RouteType } from '../../HttpRequest';
import type { SocketModuleTypes } from '../PushEventName'
import { useMountCommon, useMemoizedFnCommon } from '@kiki/f2e.khooks.common';
import MarketSymbolsCache from '../MarketSymbolsCache';
import { HttpLogcater } from '../../JsSdkkLogCat';
import { JsSdkEventEmitInstance } from '../../SdkInitApi';
import SocketEventManager from '../SocketEventManager';
import type JsSdkEventEmit from '../../utils/JsSdkEventEmit';
import type AbortSignal from '../../utils/AbortSignal';

function getCommonSocketConnectFuncHoc(routeObj: RouteType, aliasName: SocketModuleTypes, abortSignal?: AbortSignal) {
    const realSocketConnect = getConnectFunc(aliasName)

    return async function getCommonSocketConnect(...options: any[]) {
        const scopeId = await SockcetScopeIdManager.getScopeIdByRouteKey(routeObj?.key, abortSignal)
        // println("socketscopeid--connectOrderBook ==== $scope")

        // 可能会被abortSignal打断，所以进行判断
        if(!!scopeId){
            // @ts-expect-error
            const pushEventId = await realSocketConnect(scopeId, ...options)
            if(abortSignal?.isAborted){
                SockcetScopeIdManager.deleteSingleScopeId(scopeId)
                return null
            }
            return pushEventId
        }
        return null
    }
}

export function useConnectSocket(aliasName: SocketModuleTypes, pageEventEmitter: JsSdkEventEmit, ...options: any[]) {
    const routeObj = useSafeRoute()
    const hasConnectRef = useRef(false)
    const reconnectOnDbReadyRef = useRef(null)
    const pushEventLisRef = useRef(null)

    const addPushLis = useMemoizedFnCommon((pushEventId: string) => {
        console.log("httpLogcaterTest:-addPushLis", pushEventId)

        // @ts-expect-error
        pushEventLisRef.current?.()
        pushEventLisRef.current = null
        // @ts-expect-error
        pushEventLisRef.current = SocketEventManager.addChildEventLis(pushEventId, (eventData) => {
            console.log("httpLogcaterTest:-pushsiokqk", pageEventEmitter, aliasName, eventData.data)
            pageEventEmitter.emitEvent(aliasName, eventData)

            const startTime = new Date().getTime()
            //兼容redux
            if (aliasName === 'socket.marketsymbol') {
                JsSdkEventEmitInstance.emitEvent('jssdk_marketsysbols_push', eventData.data)
            }
            const endTime = new Date().getTime()
            console.log("httpLogcaterTest:-socket同步redux耗时", endTime - startTime)
        })
    })

    const createConnect = useMemoizedFnCommon(async (abortSignal: AbortSignal) => {

        // 数据库初始化完成
        if (MarketSymbolsCache.getHasInit() && !hasConnectRef.current) {
            hasConnectRef.current = true
            reconnectOnDbReadyRef.current = null
            const pushEventId = await getCommonSocketConnectFuncHoc(routeObj, aliasName, abortSignal)(...options)
            pushEventId && addPushLis(pushEventId)
        }
        if (!MarketSymbolsCache.getHasInit()) {
            // @ts-expect-error
            reconnectOnDbReadyRef.current = async () => {
                hasConnectRef.current = true
                reconnectOnDbReadyRef.current = null
                const pushEventId = await getCommonSocketConnectFuncHoc(routeObj, aliasName)(...options)
                pushEventId && addPushLis(pushEventId)
            }
        }
    })

    const destoryConnect = useCallback(() => {
        hasConnectRef.current = false
        console.log("SocketPageControl-useConnectSocket--destory", aliasName, routeObj?.key)
        SockcetScopeIdManager._deleteScopeId(routeObj?.key)
        // @ts-expect-error
        pushEventLisRef.current?.()
        pushEventLisRef.current = null
    }, [routeObj?.key])

    useMountCommon(() => {
        let initCacheSuccessLis: (() => void) | null = null
        // 数据库没有初始化完成，需要加监听
        if (!MarketSymbolsCache.getHasInit()) {
            initCacheSuccessLis = JsSdkEventEmitInstance.addEventListener('jssdk_initcache_success', () => {
                HttpLogcater.httpLogcaterTest("useConnectSocket", 'db初始化成功,尝试链接', !!reconnectOnDbReadyRef.current)

                // @ts-expect-error
                reconnectOnDbReadyRef.current?.()
                HttpLogcater.httpLogcaterTest("useConnectSocket", '监听时机才链接')
                initCacheSuccessLis?.()
            })
        }
        return () => {
            initCacheSuccessLis?.()
        }
    })


    return [createConnect, destoryConnect]
}
