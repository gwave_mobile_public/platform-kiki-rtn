
/**
 * sockt 统一事件名称
 */
import { MarketDeviceEventEmitter } from '../../SocketModule'
import JsSdkEventEmit from '../../utils/JsSdkEventEmit'

const SocketEventEmit = new JsSdkEventEmit()

const REMOTE_DATE_UPDATE_EVENT = "@marketConnectToNativeEvent"

class SocketEventManager {

    addParentEventLis = () => {
        return MarketDeviceEventEmitter.addListener(REMOTE_DATE_UPDATE_EVENT, (remoteData: { eventUpdateEventId: string, data: any, isSuccess: boolean }) => {
            const { eventUpdateEventId } = remoteData
            console.log("httpLogcaterTest:-socketxx", eventUpdateEventId, Object.keys(remoteData))
            SocketEventEmit.emitEvent(eventUpdateEventId, { data: remoteData.data, isSuccess: remoteData.isSuccess })
        })
    }

    addChildEventLis = (eventUpdateEventId: string, callBack: (eventData: any) => void) => {
        if (!!eventUpdateEventId) {
            const removeCallBack = SocketEventEmit.addEventListener(eventUpdateEventId, callBack)
            return removeCallBack
        }
        return null
    }

    removeAllRemoteHttpLis = () => {
        SocketEventEmit.removeAllListener()
    }
}


export default new SocketEventManager()
