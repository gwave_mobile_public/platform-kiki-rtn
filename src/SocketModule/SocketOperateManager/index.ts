
import { NormalScopeIdManager } from '../../ScopeManager';
import type { RouteType } from '../../HttpRequest';
import type { SocketOperateModuleTypes } from '../PushEventName'
import { getOperateFunc } from '../PushEventName'

export function createSocketOpetate(routeObj: RouteType, aliasName: SocketOperateModuleTypes) {
    const realOperateFunc = getOperateFunc(aliasName)
    return async function getCommonSocketConnect(...options: any[]) {
        const scopeId = await NormalScopeIdManager.getScopeIdByRouteKey(routeObj?.key)
        // @ts-expect-error
        realOperateFunc(scopeId, ...options)
    }
}
