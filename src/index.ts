

import { SocketAutoConnect, useJsSdkSocketListener } from './PageConnect'
import {createSocketOpetate} from './SocketModule'
import {JsSdkEventEmitInstance} from './SdkInitApi'
export {
    // socket相关api
    SocketAutoConnect,
    JsSdkEventEmitInstance,
    useJsSdkSocketListener,
    createSocketOpetate
}
