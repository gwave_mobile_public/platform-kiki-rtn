

class AbortSignal {
    isAborted: boolean = false
    constructor() {
        this.isAborted = false
    }

    abort() {
        this.isAborted = true
    }
}

export default AbortSignal