
import EventEmitter from 'eventemitter3'


class JsSdkEventEmit {
    remoteHttpEmitter: EventEmitter;
    constructor() {
        this.remoteHttpEmitter = new EventEmitter();
    }

    addEventListener = (eventName: string, callBack: (event: any) => void): { (): void } => {
        this.remoteHttpEmitter.addListener(eventName, callBack)
        return () => {
            this.remoteHttpEmitter.removeListener(eventName, callBack)
        }
    }

    emitEvent = (eventName: string, eventData: any) => {
        this.remoteHttpEmitter.emit(eventName, eventData)
    }

    removeAllListener = () => {
        this.remoteHttpEmitter.removeAllListeners()
    }
}

export default JsSdkEventEmit

export const JsSdkEventEmitInstance = new JsSdkEventEmit()